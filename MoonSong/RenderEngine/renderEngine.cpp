
/*
*  MoonSong - Conundrum
*  renderEngine.cpp
*  Last Reviewed: 08/05/2014
*/

#include "renderEngine.h"

/*
*  Structs Required.
*/
rt3d::lightStruct light0 = {
	{1.0f, 1.0f, 1.0f, 1.0f},
	{1.0f, 1.0f, 1.0f, 1.0f},
	{1.0f, 1.0f, 1.0f, 1.0f},
};

rt3d::materialStruct material0 = {
	{0.5f, 0.5, 0.5, 1.0},
	{0.5f, 0.5, 0.5, 1.0},
	{1.0f, 1.0, 1.0, 1.0},
	{8.0f}
};

#pragma region Constructor, Deconstructor, set ups and Init
/*
*  Constructor
*  Sets variables to required values at program start.
*/
RenderEngine::RenderEngine()
{
	gameOver = false;

	meshIndexCountCube = 0;
	meshIndexCountSphere = 0;
	meshIndexCountSpaceShip = 0;
	meshIndexCountEnemy = 0;
	meshIndexCountRocket = 0;

	r = 0.0f;
	yR = 0.0f;
	meshCount = 0;

	attConstant = 1.0f;
	attLinear = 0.000000001f;
	attQuadratic = 0.000000001f;

	bitmapLoader = new BitmapLoader();
	buyMenu = false;

	boundingSet = false;
	laserFire = false;
	pauseQuit = false;

	shop1 = new Shop("inID", "UI/shop1/Shop.txt","UI/shop1/weapons.txt","UI/shop1/commodities.txt");
	shop2 = new Shop("inID", "UI/shop2/Shop.txt","UI/shop2/weapons.txt","UI/shop2/commodities.txt");

	pauseShop = new Shop("inID", "UI/pauseMenu.txt", "UI/Options.txt" ,"UI/saves.txt");
	gamePaused = false;

	mouseSensitivity = 0.4;
	mouseSensitivity = 0.4;
	mouseControl = true;
}

/*
*  Deconstructor.
*/
RenderEngine::~RenderEngine()
{
	DeleteObjects();
}

/*
* Loads Cube map.
* @param - char - sides of cube.
* @param - GLuint - Textures of cube.
*/
GLuint RenderEngine::LoadCubeMap(const char *fname[6], GLuint *texID)
{
	glGenTextures(1, texID); // generate texture ID
	GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y	};
	SDL_Surface *tmpSurface;

	glBindTexture(GL_TEXTURE_CUBE_MAP, *texID); // bind texture and set parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	GLuint externalFormat;
	for (int i=0;i<6;i++)
	{
		// load file - using core SDL library
		tmpSurface = SDL_LoadBMP(fname[i]);
		if (!tmpSurface)
		{
			std::cout << "Error loading bitmap" << std::endl;
			return *texID;
		}

		// skybox textures should not have alpha (assuming this is true!)
		SDL_PixelFormat *format = tmpSurface->format;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;

		glTexImage2D(sides[i],0,GL_RGB,tmpSurface->w, tmpSurface->h, 0,
			externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		// texture loaded, free the temporary buffer
		SDL_FreeSurface(tmpSurface);
	}
	return *texID;	// return value of texure ID, redundant really

}

/*
*  Creates text on to a texture.
*  @param - char - the text to be put on the texture.
*/
GLuint RenderEngine::TextToTexture(const char * str)
{
	TTF_Font *font = textFont;
	SDL_Color colour = { 255, 255, 255 };
	SDL_Color bg = { 0, 0, 0 };

	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(font,str,colour);

	if (stringImage == NULL)
		//exitFatalError("String surface not created.");
		std::cout << "String surface not created." << std::endl;

	GLuint w = stringImage->w;
	GLuint h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
		else
			format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
		else
			format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
		format, GL_UNSIGNED_BYTE, stringImage->pixels);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);
	return texture;
}

/*
*  Loads the shop.
*  @param - std::string - the ID for the shop.
*  @param - char - the name of the shop.
*/
Shop* RenderEngine::LoadShop(std::string inID, const char *fname)
{
	Shop* temp = new Shop(inID, fname, fname, fname);
	temp->Init();
	return temp;
}

/*
*  Initializes everything that is required for the renderer plus any other classes that it will use.
*/
void RenderEngine::Init()
{		
	player1 = new Player(100);
	player1->LoadStats("PlayerSaveFiles/Inventory.txt");  

	physManager = new fizz::fizzypop();
	keyManager = new fizz::fizzykeys();
	AIManager = new fizz::fizzyAIlight();

	battle_Engine = new BattleEngine();
	battle_Engine->Init();
	cam = 0;
	camDist = 7.0f;
	eye = (glm::vec3(0.0f, -1.0f, -3.0f) * camDist) * quat::eulerToQuat(0, 90, 0);

	//Textures
	textures[0] = bitmapLoader->loadBitmap("Images/Lirael.bmp");
	textures[1] = bitmapLoader->loadBitmap("Images/mercury.bmp");
	textures[2] = bitmapLoader->loadBitmap("Images/jupiter.bmp");//http://flatplanet.sourceforge.net/maps/alien.html
	textures[3] = bitmapLoader->loadBitmap("Images/pluto.bmp");//
	textures[4] = bitmapLoader->loadBitmap("Images/enemy.bmp");
	textures[5] = bitmapLoader->loadBitmap("Images/shop1.bmp");
	textures[6] = bitmapLoader->loadBitmap("Images/White.bmp");
	textures[7] = bitmapLoader->loadBitmap("UI/grey.bmp");
	hudImage = bitmapLoader->loadBitmap("UI/hud.bmp");
	healthBar = bitmapLoader->loadBitmap("UI/healthBar.bmp");
	ammoBar = bitmapLoader->loadBitmap("UI/ammoBar.bmp");
	simpleProgram = rt3d::initNoLightShaders("ShaderFiles/simple.vert", "ShaderFiles/simple.frag");
	laserFireShader = rt3d::initShaders("ShaderFiles/laserFire.vert", "ShaderFiles/laserFire.frag");

	phongShader = rt3d::initShaders("ShaderFiles/phong-tex.vert","ShaderFiles/phong-tex.frag");
	rt3d::setLight(phongShader, light0);
	rt3d::setMaterial(phongShader, material0);

	particleShader = rt3d::initShaders("ShaderFiles/particle.vert", "ShaderFiles/particle.frag");

	HUDProgram = rt3d::initShaders("ShaderFiles/HUD.vert", "ShaderFiles/HUD.frag");
	rt3d::setLight(HUDProgram, light0);

	skyboxProgram = rt3d::initShaders("ShaderFiles/cubeMap.vert", "ShaderFiles/cubeMap.frag");
	//skybox from http://forum.unity3d.com/threads/99258-Space-Skybox
	const char *cubeTexFiles[6] = {
		"Skybox/Skybox360Front.bmp", "Skybox/Skybox360Back.bmp", "Skybox/Skybox360Right.bmp", "Skybox/Skybox360Left.bmp", "Skybox/Skybox360Up.bmp", "Skybox/Skybox360Down.bmp"
	};
	LoadCubeMap(cubeTexFiles, &skybox[0]);

	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLfloat> colours;
	std::vector<GLuint> indices;

	rt3d::loadObj("cube.obj", cubeVerts, norms, tex_coords, indices);
	meshIndexCountCube = indices.size();

	//Create mesh for cube.obj
	meshObjects[0] = rt3d::createMesh(cubeVerts.size()/3, cubeVerts.data(), colours.data(), norms.data(), tex_coords.data(), meshIndexCountCube, indices.data());

	//Sphere
	norms.clear();tex_coords.clear();indices.clear();
	rt3d::loadObj("ObjFiles/Planet.obj", sphereVerts, norms, tex_coords, indices);
	meshIndexCountSphere = indices.size();
	meshObjects[1] = rt3d::createMesh(sphereVerts.size()/3, sphereVerts.data(), nullptr, norms.data(), tex_coords.data(), meshIndexCountSphere, indices.data());

	//Shop
	norms.clear();tex_coords.clear();indices.clear();
	rt3d::loadObj("ObjFiles/Shop.obj", shopVerts, norms, tex_coords, indices);
	meshIndexCountSphere = indices.size();
	meshObjects[4] = rt3d::createMesh(shopVerts.size()/3, shopVerts.data(), nullptr, norms.data(), tex_coords.data(), meshIndexCountSphere, indices.data());

	//Spaceship
	norms.clear();tex_coords.clear();indices.clear();
	rt3d::loadObj("ObjFiles/Lirael2.obj", spaceshipVerts, norms, tex_coords, indices);//"ObjFiles/ENEMYSPACESHIP.obj"
	meshIndexCountSpaceShip = indices.size();
	meshObjects[2] = rt3d::createMesh(spaceshipVerts.size()/3, spaceshipVerts.data(), nullptr, norms.data(), tex_coords.data(), meshIndexCountSpaceShip, indices.data());


	//Asteroid
	norms.clear();tex_coords.clear();indices.clear();
	rt3d::loadObj("ObjFiles/Enemy.obj", enemyVerts, norms, tex_coords, indices);
	meshIndexCountEnemy = indices.size();
	meshObjects[3] = rt3d::createMesh(enemyVerts.size()/3, enemyVerts.data(), nullptr, norms.data(), tex_coords.data(), meshIndexCountEnemy, indices.data());

	//Rocket
	norms.clear();tex_coords.clear();indices.clear();
	rt3d::loadObj("ObjFiles/Enemy.obj", enemyVerts, norms, tex_coords, indices);
	meshIndexCountEnemy = indices.size();
	meshObjects[5] = rt3d::createMesh(enemyVerts.size()/3, enemyVerts.data(), nullptr, norms.data(), tex_coords.data(), meshIndexCountEnemy, indices.data());

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		std::cout << "TTF failed to initialise." << std::endl;
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		std::cout << "Failed to open font." << std::endl;

	shop1->Init();
	shop2->Init();
	pauseShop->Init();

	std::stringstream ss;
	ss << player1->GetPlayerMoney();
	std::string inMo = ss.str();

	playerMoneyDisplay = CreateUIBox("UI",inMo,2,glm::vec2(-0.8), glm::vec2(0.15));
}

#pragma endregion

/*
*  Updates the players money.
*  @param - RenderObject - The object to be updated.
*/
void RenderEngine::UpdatePlayerMoney(RenderObject* obj)
{
	std::stringstream ss;
	ss << player1->GetPlayerMoney();
	std::string inName = ss.str();
	std::string temp = inName.c_str();
	std::string temp2 = "Credits: " + temp;
	obj->SetUITexture(TextToTexture(temp2.c_str()));
}

/*
*Sets shop to open
*/
void RenderEngine::PauseGame()
{
	pauseShop->OpenShop();
}

/*
*Sets shop to close
*/
void RenderEngine::UnpauseGame()
{
	pauseShop->CloseShop();
}

/*
*  Creates new obj from details input.
*  Creates bounding based on ID and name, name used to hold track of bounding.
*  Adds obj to render list.
*  @param - std::string - ID of object, @param - std::string - name of object, @param - std::vector - object vertices.
*  @param - float - health of object, @param - int - mass of object, @param - int - meshID of object. 
*  @param - int - textureID of object, @param - fizz::fizz - physics of object, @param - float - scale of object.
*/
void RenderEngine::CreateObject(std::string inID, std::string inName, std::vector<GLfloat> inVertices, float inHealth, int inMass, int inMeshID, int inTexID, fizz::fizz *inFizztemp, float inScale)
{
	RenderObject * obj = new RenderObject(inID, inName, inVertices, inHealth, inMass, inMeshID, inTexID, inFizztemp, inScale);	
	battle_Engine->CreateBounding(inID, inName); 
	renderList.push_back(obj);
}

/*
*  Creates the shop object.
*  @param - std::string - ID of object, @param - std::string - name of object.
*  @param - int - textureID of object, @param - glm::vec2 - position of object, @param - glm::vec2 - scale of object.
*/
RenderObject* RenderEngine::CreateUIBox(std::string inID, std::string inName, int inTexID, glm::vec2 pos, glm::vec2 inScale)
{
	RenderObject * obj = new RenderObject(inID, inName, inTexID, pos, inScale);
	obj->SetUITexture(TextToTexture(inName.c_str()));
	return obj;	
}

/*
*  Renders the game shop.
*  @param - shop - the shop to render.
*  @param - glm::mat4 - the matrix for rendering.
*/
void RenderEngine::RenderGameUI(Shop inShop, float xOffset, float yOffset, std::stack<glm::mat4> inStack)
{
	glm::mat4 projection = glm::ortho<GLfloat>(0.0f, 800.0f, 600.0f, 0.0f, 1.0f, -0.1f );
	std::vector<RenderObject *> inList = inShop.ReturnUIList();
	for(int i=0; i<inList.size(); i++)
	{
		RenderObject  * obj = inList[i];
		if(obj->IsVisible() == true)
		{
			glUseProgram(HUDProgram);
			glBindTexture(GL_TEXTURE_2D, obj->ReturnUITexure());
			inStack.push(glm::mat4(1.0));
			inStack.top() = glm::translate(inStack.top(), glm::vec3(obj->GetUIPosition()[0]+xOffset,obj->GetUIPosition()[1] + yOffset,-1.0));
			inStack.top() = glm::scale(inStack.top(), glm::vec3(obj->GetUIScale(), 0.15));
			rt3d::setUniformMatrix4fv(HUDProgram, "projection", glm::value_ptr(projection));
			rt3d::setUniformMatrix4fv(HUDProgram, "modelview", glm::value_ptr(inStack.top()));
			rt3d::setMaterial(HUDProgram, material0);
			rt3d::drawIndexedMesh(meshObjects[0],meshIndexCountCube,GL_TRIANGLES);
			inStack.pop();	
		}
	}
}

/*
*  Gets the requested object and returns it.
*  @param - std::string - the name of the object.
*  @return - RenderObject - the object to return.
*/
RenderObject *RenderEngine::GetObj(std::string name)
{
	bool found = false;
	int i = 0;
	while(i < renderList.size() && found != true)
	{
		if(renderList[i]->GetName() == name)
			found = true;
		else 
			i++;
	}	
	if(found == false)
		return NULL;
	else
		return renderList[i];	
}

/*
* Searches for and then deletes the object from name taken in.
* @param - std::string - the name of the object to be deleted.
*/
void RenderEngine::DeleteObj(std::string name)
{
	bool found = false;
	int i = 0;
	while(i < renderList.size() && found != true)
	{
		if(renderList[i]->GetName() == name)
			found = true;
		else 
			i++;
	}	
	if(found == false)
		std::cout << "Name doesn't exist" << std::endl;
	else
	{
		delete renderList[i];
		renderList[i] = NULL;
		renderList.erase(renderList.begin() + i);
	}
}

/*
*  Deletes all objects held in the renderList.
*/
void RenderEngine::DeleteObjects()
{
	if(renderList.size() > 0)
	{		
		for(int i = 0; i < renderList.size(); ++i)
		{
			delete renderList[i];
			renderList[i] = NULL;
		}			
		renderList.clear();			
	}
}

/*
*  Renders all objects held in the renderList. Will also update the bounding based on the objects position.
*  @param - std::vector - the list to render.
*  @param - std::stack - stack that holds the matrix.
*/
void RenderEngine::RenderObjectList(std::vector<RenderObject *> inList, std::stack<glm::mat4> inStack)
{
	for(int i=0; i<inList.size(); i++){
		RenderObject  * obj = inList[i];

		if(obj->IsVisible() == true && obj->GetHealth() > 0)
		{
			GLuint tempShader;
			if(obj->GetShader() == NULL)
				tempShader = phongShader;
			else 
				tempShader = obj->GetShader();
			glUseProgram(tempShader);
			float scale = obj->GetScale();
			glBindTexture(GL_TEXTURE_2D, textures[obj->GetTextureID()]);
			rt3d::materialStruct tmpMaterial = material0;
			rt3d::setMaterial(tempShader, tmpMaterial);
			inStack.push(inStack.top());

			//Required to update vertices of object for bounding. Only require scaling/rotation operations.
			glm::mat4 boundingMatrix;
			boundingMatrix *= quat::quatToMat4( quat::scale( obj->GetRotation(), -1.0f ) );
			boundingMatrix = glm::scale(boundingMatrix,glm::vec3(obj->GetScale()));

			inStack.top() = glm::translate(inStack.top(),obj->GetPosition());
			inStack.top() *= quat::quatToMat4( quat::scale( obj->GetRotation(), -1.0f ) );
			inStack.top() = glm::scale(inStack.top(),glm::vec3(obj->GetScale()));
			GLuint meshIndexCount;
			if(obj->GetMeshID() == 1) //Renders Spheres(Planets)
			{
				if(boundingSet == false)
				{
					obj->UpdateVertices(boundingMatrix);		
					battle_Engine->GetSphere(obj->GetName())->SetSB(obj->GetVertexData(), obj->GetVertexData().size());
					battle_Engine->GetSphere(obj->GetName())->UpdatePosition(obj->GetPosition());
				}
				meshIndexCount = meshIndexCountSphere;
			}
			if(obj->GetMeshID() == 2)
			{
				if(boundingSet == false)
				{
					obj->UpdateVertices(boundingMatrix);		
					battle_Engine->GetBox(obj->GetName())->SetBB(obj->GetVertexData(), obj->GetVertexData().size());					
				}					
				obj->SetTexture(0);
				meshIndexCount = meshIndexCountSpaceShip;
				battle_Engine->GetBox(obj->GetName())->UpdatePosition(obj->GetPosition()); 
			}
			if(obj->GetMeshID() == 3) // enemies
			{
				if(boundingSet == false)
				{
					obj->UpdateVertices(boundingMatrix);		
					battle_Engine->GetBox(obj->GetName())->SetBB(obj->GetVertexData(), obj->GetVertexData().size());					
				}		
				obj->SetTexture(4);
				meshIndexCount = meshIndexCountEnemy;
				battle_Engine->GetBox(obj->GetName())->UpdatePosition(obj->GetPosition());
			}
			if(obj->GetMeshID() == 5) // rockets
			{
				if(boundingSet == false)
				{
					obj->UpdateVertices(boundingMatrix);		
					battle_Engine->GetBox(obj->GetName())->SetBB(obj->GetVertexData(), obj->GetVertexData().size());					
				}		
				obj->SetTexture(1);
				meshIndexCount = meshIndexCountEnemy;
				battle_Engine->GetBox(obj->GetName())->UpdatePosition(obj->GetPosition());
			}
			if(obj->GetMeshID() == 4) //Renders Spheres(shops)
			{
				if(boundingSet == false)
				{
					obj->UpdateVertices(boundingMatrix);		
					battle_Engine->GetSphere(obj->GetName())->SetSB(obj->GetVertexData(), obj->GetVertexData().size());
					battle_Engine->GetSphere(obj->GetName())->UpdatePosition(obj->GetPosition());
				}
				meshIndexCount = meshIndexCountSphere;
			}
			rt3d::setUniformMatrix4fv(tempShader, "modelview", glm::value_ptr(inStack.top()));
			rt3d::drawMesh(meshObjects[obj->GetMeshID()], meshIndexCount, GL_TRIANGLES);
			inStack.pop();
		}
		else
		{
			if(obj->GetID() == "ENEMY")
			{
				char trackerNum = obj->GetName()[5];				
				bool found = false;
				int index = 0;				
				RenderObject * obj2 = inList[index];
				std::string str = "Tracker";
				str += trackerNum;
				while(found != true)
				{
					if(str == obj2->GetName())
						found = true;
					else
						obj2 = inList[index++];
				}
				battle_Engine->DeleteBounding(obj2->GetID(), obj2->GetName());
				DeleteObj(obj2->GetName());				
			}
			if(obj->GetName() == "Player")
			{
				gameOver = true;
			}						
			battle_Engine->DeleteBounding(obj->GetID(), obj->GetName());
			DeleteObj(obj->GetName());
		}
	}	
}

/*
*  Opens buy menu.
*/
void RenderEngine::OpenBuyMenu()
{
	buyMenu = true;
}

/*
*  Closes buy menu.
*/
void RenderEngine::CloseBuyMenu()
{
	buyMenu = false;
}

/*
*  Updates any changes that have been made to various variables/functions.
*  @param - float - radius.
*/
void RenderEngine::Update(float r)
{
	keyManager->update( (unsigned char*)SDL_GetKeyboardState(NULL) );

	if(!pauseShop->ReturnOpen()){physManager->update();}

	player1->UpdateHealth(renderList[0]->GetHealth());
	glm::quat orientation = renderList[cam]->GetFizz()->getOri();

	camera = quat::rotate( orientation, quat::eulerToQuat(0, 90+r, 0) );

	glm::vec3 position = renderList[cam]->GetPosition();
	glm::vec3 velocity = renderList[cam]->GetFizz()->getVel();

	SDL_GetMouseState(&mouseX,&mouseY);
	floatMouseX = mouseX;
	floatMouseY = mouseY;
	floatMouseX = (floatMouseX/400 - 1);
	floatMouseY = -(floatMouseY/300 - 1);

	renderList[0]->SetAngle(quat::eulerToQuat(0, 0, 1));

	eye = ( (glm::vec3(0.0f, -1.0f, -3.0f) * camDist) * camera ) - position + (velocity * 5.0f * (1/(glm::length(velocity)+1)) );
	at.x += mouseX;
	
	if(buyMenu)
	{
		shop1->UpdatePlayerMoney(player1->GetPlayerMoney());
		shop1->RunUI();
		UpdatePlayerMoney(playerMoneyDisplay);
	}

	if(pauseShop->ReturnOpen()){
		pauseShop->UpdatePlayer(player1);
		pauseShop->RunPause();
		pauseQuit = pauseShop->ReturnQuit();
	}
}

/*
*  Renders the skybox.
*  @param - std::stack - stack that holds the matrix.
*/
void RenderEngine::RenderSkybox(std::stack<glm::mat4> mvStack)
{
	glm::mat4 projection(1.0);//duplicate code
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,150.0f);//duplicate code
	glUseProgram(skyboxProgram);
	rt3d::setUniformMatrix4fv(skyboxProgram, "projection", glm::value_ptr(projection));
	glDepthMask(GL_FALSE);
	glm::mat3 mvRotOnlyMat3 = glm::mat3(mvStack.top());
	mvStack.push( glm::mat4(mvRotOnlyMat3) );
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, skybox[1]);
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(1.5f, 1.5f, 1.5f));
	rt3d::setUniformMatrix4fv(skyboxProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCountCube,GL_TRIANGLES);
	mvStack.pop();
	glCullFace(GL_BACK);
	glDepthMask(GL_TRUE);
}

/*
*  Renders text.
*  @param - renderObject - the object to be used.
*  @param - std::stack - stack that holds the matrix.
*/
void RenderEngine::RenderText(RenderObject* inObj, std::stack<glm::mat4> inStack)
{
	glm::mat4 projection = glm::ortho<GLfloat>(0.0f, 800.0f, 600.0f, 0.0f, 1.0f, -0.1f );
	glUseProgram(HUDProgram);
	glBindTexture(GL_TEXTURE_2D, inObj->ReturnUITexure());
	inStack.push(glm::mat4(1.0));
	inStack.top() = glm::translate(inStack.top(), glm::vec3(inObj->GetUIPosition()[0],inObj->GetUIPosition()[1],-1.0));
	inStack.top() = glm::scale(inStack.top(), glm::vec3(inObj->GetUIScale(), 0.15));
	rt3d::setUniformMatrix4fv(HUDProgram, "projection", glm::value_ptr(projection));
	rt3d::setUniformMatrix4fv(HUDProgram, "modelview", glm::value_ptr(inStack.top()));
	rt3d::setMaterial(HUDProgram, material0);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCountCube,GL_TRIANGLES);
	inStack.pop();
}

/*
*  Renders text.
*  @param - renderObject - the object to be used.
*  @param - std::stack - stack that holds the matrix.
*/
void RenderEngine::RenderTextNew(const char* inText, std::stack<glm::mat4> inStack, glm::vec3 inTranslate, glm::vec3 inScale)
{
	glm::mat4 projection = glm::ortho<GLfloat>(0.0f, 800.0f, 600.0f, 0.0f, 1.0f, -0.1f );
	glUseProgram(HUDProgram);
	glBindTexture(GL_TEXTURE_2D, TextToTexture(inText));
	inStack.push(glm::mat4(1.0));
	inStack.top() = glm::translate(inStack.top(), inTranslate);
	inStack.top() = glm::scale(inStack.top(), inScale);
	rt3d::setUniformMatrix4fv(HUDProgram, "projection", glm::value_ptr(projection));
	rt3d::setUniformMatrix4fv(HUDProgram, "modelview", glm::value_ptr(inStack.top()));
	rt3d::setMaterial(HUDProgram, material0);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCountCube,GL_TRIANGLES);
	inStack.pop();
}

/*
*  Renders the UI image
*  @param - int - the number of the image.
*  @param - std::stack - stack that holds the matrix.
*/
void RenderEngine::RenderUIImage(GLuint inImage, std::stack<glm::mat4> inStack, glm::vec3 inTranslate, glm::vec3 inScale)
{
	glm::mat4 projection = glm::ortho<GLfloat>(0.0f, 800.0f, 600.0f, 0.0f, 1.0f, -0.1f );
	glUseProgram(HUDProgram);
	glBindTexture(GL_TEXTURE_2D, inImage);
	inStack.push(glm::mat4(1.0));
	inStack.top() = glm::translate(inStack.top(), inTranslate);
	inStack.top() = glm::scale(inStack.top(), inScale);
	rt3d::setUniformMatrix4fv(HUDProgram, "projection", glm::value_ptr(projection));
	rt3d::setUniformMatrix4fv(HUDProgram, "modelview", glm::value_ptr(inStack.top()));
	rt3d::setMaterial(HUDProgram, material0);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCountCube,GL_TRIANGLES);
	inStack.pop();
}

/*
*  Sets laserFire to true if key is pressed.
*/
void RenderEngine::Fire()
{
	laserFire = true;
}

/*
*  Main Draw function.
*  @param - SDL_Window - The window to draw to.
*  @param - std::stack - stack that holds the matrix.
*/
void RenderEngine::Draw(SDL_Window * window, std::stack<glm::mat4> mvStack)
{		
	glEnable(GL_CULL_FACE);
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,1000000.0f);
	GLfloat scale(1.0f); 
	glm::mat4 modelview(1.0);
	mvStack.push(modelview);	

	//Mouse reset
	if(!buyMenu && !pauseShop->ReturnOpen()){SDL_WarpMouseInWindow(window, 400, 300);}

	mvStack.top() = quat::quatToMat4(camera); 
	mvStack.top() = glm::translate( mvStack.top(), eye );
	glm::vec4 tmp = mvStack.top()*lightPos;

	RenderSkybox(mvStack);	

	glUseProgram(textureMultipleLights);
	glBindTexture(GL_TEXTURE_2D, textures[0]);

	glUseProgram(phongShader);
	glBindTexture(GL_TEXTURE_2D, textures[0]);

	// set light attenuation shader uniforms
	int uniformIndex = glGetUniformLocation(phongShader, "attConst");
	glUniform1f(uniformIndex, attConstant);
	uniformIndex = glGetUniformLocation(phongShader, "attLinear");
	glUniform1f(uniformIndex, attLinear);
	uniformIndex = glGetUniformLocation(phongShader, "attQuadratic");
	glUniform1f(uniformIndex, attQuadratic);

	rt3d::setUniformMatrix4fv(phongShader, "projection", glm::value_ptr(projection));
	lightPos = glm::vec4(renderList[2]->GetPosition(), 1.0);
	rt3d::setLightPos(phongShader, glm::value_ptr(lightPos));

	RenderObjectList(renderList, mvStack);

	//Object Collision Detection	
	battle_Engine->CheckCollision(renderList);	

	//Stops bounding being reset as bounding moves with player does not need to be changed after creation.
	boundingSet = true;

	//Checks laserFire collision when fired.
	if(laserFire == true && player1->ReturnPrimaryAmmo() > 0)
	{		
		RenderObject * player = GetObj("Player");
		if(player->GetLaserDisabled() == false)
		{
			glUseProgram(laserFireShader);		
			glm::vec4 modelLookAt = glm::vec4(1, 0, 0, 1.0) * player->GetRotation();		
			glm::vec3 playerLookAt = glm::vec3(modelLookAt.x, modelLookAt.y, modelLookAt.z);
			//Find far away point on line using the line equation Point(t) = S(initial point) + V(line direction)t(parameter)
			//Value of t should be high enough to create a point where laser fire will disappear.
			glm::vec3 endFirePos = player->GetPosition() + (500.0f * playerLookAt);	

			endFirePos = battle_Engine->HitScan(player->GetPosition(), playerLookAt, endFirePos, renderList, *player);

			GLfloat verts[] = {player->GetPosition().x, player->GetPosition().y, player->GetPosition().z, endFirePos.x, endFirePos.y, endFirePos.z};
			GLfloat colours[] = {player1->GetFireColour().x, player1->GetFireColour().y, player1->GetFireColour().z, player1->GetFireColour().x, player1->GetFireColour().y, player1->GetFireColour().z};

			GLuint meshFire = rt3d::createMesh(2, verts, colours, nullptr, nullptr, 2, nullptr);
			rt3d::setUniformMatrix4fv(laserFireShader, "projection", glm::value_ptr(projection));
			rt3d::setUniformMatrix4fv(laserFireShader, "modelview", glm::value_ptr(mvStack.top()));
			glLineWidth(player1->GetFireSize());
			rt3d::drawMesh(meshFire, 6, GL_LINES);
			player1->ShootPrimary();
		}
		else
		{
			player->SetLaserStart(clock());			
			if(player->GetLaserStart() > player->GetLaserFinish())
				player->SetLaserDisabled(false);
		}

	}	
	//Sets laserFire to false as already checked. Also will not be checked next call unless firing again.
	laserFire = false;

	if(GetObj("Enemy1") != NULL)
	{			
		RenderObject * enemy1 = GetObj("Enemy1");
		enemy1->SetLaserFireStart(clock());
		enemy1->SetLaserStart(clock());			
		if(enemy1->GetLaserStart() > enemy1->GetLaserFinish())
			enemy1->SetLaserDisabled(false);
		if(enemy1->GetLaserFireStart() > enemy1->GetLaserFire() && enemy1->GetLaserFireStart() < enemy1->GetLaserFinish())
		{
			if(enemy1->GetLaserDisabled() == false)
			{
				glUseProgram(laserFireShader);		
				glm::vec4 modelLookAt = glm::vec4(1, 0, 0, 1.0) * enemy1->GetRotation();		
				glm::vec3 enemyLookAt = glm::vec3(modelLookAt.x, modelLookAt.y, modelLookAt.z);
				//Find far away point on line using the line equation Point(t) = S(initial point) + V(line direction)t(parameter)
				//Value of t should be high enough to create a point where laser fire will disappear.
				glm::vec3 endFirePos = enemy1->GetPosition() + (500.0f * enemyLookAt);	

				endFirePos = battle_Engine->HitScan(enemy1->GetPosition(), enemyLookAt, endFirePos, renderList, *enemy1);

				GLfloat verts[] = {enemy1->GetPosition().x, enemy1->GetPosition().y, enemy1->GetPosition().z, endFirePos.x, endFirePos.y, endFirePos.z};
				GLfloat colours[] = {enemy1->GetFireColour().x, enemy1->GetFireColour().y, enemy1->GetFireColour().z, enemy1->GetFireColour().x, enemy1->GetFireColour().y, enemy1->GetFireColour().z};

				GLuint meshFire = rt3d::createMesh(2, verts, colours, nullptr, nullptr, 2, nullptr);
				rt3d::setUniformMatrix4fv(laserFireShader, "projection", glm::value_ptr(projection));
				rt3d::setUniformMatrix4fv(laserFireShader, "modelview", glm::value_ptr(mvStack.top()));
				glLineWidth(enemy1->GetFireSize());
				rt3d::drawMesh(meshFire,6,GL_LINES);
			}			
		}
		if(enemy1->GetLaserFireStart() > enemy1->GetLaserFinish())
		{			
			enemy1->SetLaserFire(rand() % 3000 + (enemy1->GetLaserStart() + 3000)); 
			enemy1->SetLaserFinish(rand() % 3000 + (enemy1->GetLaserFire() + 1000));
		}
	}
	if(GetObj("Enemy2") != NULL)
	{
		RenderObject * enemy2 = GetObj("Enemy2");
		enemy2->SetLaserFireStart(clock());
		enemy2->SetLaserStart(clock());			
		if(enemy2->GetLaserStart() > enemy2->GetLaserFinish())
			enemy2->SetLaserDisabled(false);
		if(enemy2->GetLaserFireStart() > enemy2->GetLaserFire() && enemy2->GetLaserFireStart() < enemy2->GetLaserFinish())
		{
			if(enemy2->GetLaserDisabled() == false)
			{
				glUseProgram(laserFireShader);		
				glm::vec4 modelLookAt = glm::vec4(1, 0, 0, 1.0) * enemy2->GetRotation();		
				glm::vec3 enemyLookAt = glm::vec3(modelLookAt.x, modelLookAt.y, modelLookAt.z);
				//Find far away point on line using the line equation Point(t) = S(initial point) + V(line direction)t(parameter)
				//Value of t should be high enough to create a point where laser fire will disappear.
				glm::vec3 endFirePos = enemy2->GetPosition() + (500.0f * enemyLookAt);	

				endFirePos = battle_Engine->HitScan(enemy2->GetPosition(), enemyLookAt, endFirePos, renderList, *enemy2);

				GLfloat verts2[] = {enemy2->GetPosition().x, enemy2->GetPosition().y, enemy2->GetPosition().z, endFirePos.x, endFirePos.y, endFirePos.z};
				GLfloat colours2[] = {enemy2->GetFireColour().x, enemy2->GetFireColour().y, enemy2->GetFireColour().z, enemy2->GetFireColour().x, enemy2->GetFireColour().y, enemy2->GetFireColour().z};

				GLuint meshFire2 = rt3d::createMesh(2, verts2, colours2, nullptr, nullptr, 2, nullptr);
				rt3d::setUniformMatrix4fv(laserFireShader, "projection", glm::value_ptr(projection));
				rt3d::setUniformMatrix4fv(laserFireShader, "modelview", glm::value_ptr(mvStack.top()));
				glLineWidth(enemy2->GetFireSize());
				rt3d::drawMesh(meshFire2,6,GL_LINES);
			}			
		}
		if(enemy2->GetLaserFireStart() > enemy2->GetLaserFinish())
		{			
			enemy2->SetLaserFire(rand() % 3000 + (enemy2->GetLaserStart() + 3000)); 
			enemy2->SetLaserFinish(rand() % 3000 + (enemy2->GetLaserFire() + 1000));
		}
	}


	const Uint8 *keys = SDL_GetKeyboardState(NULL);	
	//stops the players ship.
	if ( keys[SDL_SCANCODE_V] )
	{		
		renderList[0]->SetVelocity(glm::vec3(renderList[0]->GetVelocity().x - renderList[0]->GetVelocity().x, renderList[0]->GetVelocity().y - renderList[0]->GetVelocity().y, renderList[0]->GetVelocity().z - renderList[0]->GetVelocity().z));
	} 
	//Will stop mouse control if true so keyboard controls can be used or vice versa.
	if ( keys[SDL_SCANCODE_B] )
	{
		if(mouseControl == true)
			mouseControl = false;
		else
			mouseControl = true;
	}

	//Mouse controls	
	if(!buyMenu && !pauseShop->ReturnOpen())
	{
		SDL_ShowCursor(0);

		RenderUIImage(ammoBar,mvStack,glm::vec3(0.525,0.9,0.0),glm::vec3(0.3*(player1->ReturnPrimaryAmmo()/1000.0), 0.03, 0.15));
		RenderUIImage(healthBar,mvStack,glm::vec3(-0.525,0.9,0.0),glm::vec3(0.3*(renderList[0]->GetHealth()/1000.0), 0.03, 0.15));
		RenderUIImage(hudImage,mvStack,glm::vec3(0.0),glm::vec3(1.0,1.0,0.15));

		if(mouseControl == true)
		{
			if(floatMouseX < 0.0){mouseRotation.y += -mouseSensitivity;}
			if(floatMouseX > 0){mouseRotation.y += mouseSensitivity;}
			if(floatMouseY < 0){mouseRotation.z += mouseSensitivity;}
			if(floatMouseY > 0){mouseRotation.z += -mouseSensitivity;}
			if(floatMouseX < 0.1 && floatMouseX > -mouseSensitivity){mouseRotation.y = mouseRotation.y/1.2;} 
			if(floatMouseY < 0.1 && floatMouseY > -mouseSensitivity){mouseRotation.z = mouseRotation.z/1.2;} 	
			renderList[0]->SetAngle(quat::eulerToQuat(mouseRotation.x,mouseRotation.y,mouseRotation.z));
		}
	}
	else 
		RenderUIImage(textures[7], mvStack, glm::vec3(0.0),glm::vec3(1.0,1.0,0.15));

	if(buyMenu == true)
	{
		RenderGameUI(*shop1,0.0,0.0,mvStack);
		RenderText(playerMoneyDisplay,mvStack);
		RenderUIImage(shop1->ReturnUIImage(shop1->ReturnUIImageNo()), mvStack, glm::vec3(0.5,0.0,-1.0), glm::vec3(0.45, 0.90, 0.15));
		shop1->SetShopIcon(false);
		player1 = shop1->ReturnPlayer();//Updates player1
	}

	if(pauseShop->ReturnOpen())
	{
		RenderGameUI(*pauseShop,0.7,-0.4,mvStack);
	}

	glDepthMask(GL_TRUE);	
	SDL_GL_SwapWindow(window); // swap buffers
}
