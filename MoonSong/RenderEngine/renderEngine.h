#pragma once

/*
*  MoonSong - Conundrum
*  renderEngine.h
*  Last Reviewed: 08/05/2014
*/

#include "../RT3D/rt3d.h"
#include "../RT3DObjLoader/rt3dObjLoader.h"
#include <stack>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "../RenderObject/renderObject.h"
#include <SDL_ttf.h>
#include "../BattleEngine/BattleEngine.h"
#include "../fizzypop/fizzypop.h"
#include "../fkeys/fkeys.h"
#include "../fail/fail.h"
#include"../BitmapLoader/BitmapLoader.h"
#include "../Shop/shop.h"
#include "../Player/player.h"
#include <ctime>

class RenderEngine
{
public:	
	RenderEngine();
	~RenderEngine(void);
	void Init();
	void RenderText(RenderObject* inObj, std::stack<glm::mat4> mvStack);
	GLuint RenderEngine::TextToTexture(const char * str/*, TTF_Font *font, SDL_Color colour, GLuint &w,GLuint &h*/ );	
	GLuint LoadBitmap(const char *fname);
	void RenderSkybox(std::stack<glm::mat4> mvStack);
	GLuint LoadCubeMap(const char *fname[6], GLuint *texID);
	void RenderObjectList(std::vector<RenderObject *> inList, std::stack<glm::mat4> inStack);//pass in array once tested with single
	void Draw(SDL_Window * window, std::stack<glm::mat4> getStack);
	void Update(float);		
	glm::vec3 ReturnEye(){return eye;}
	
	//Render public variables
	float camDist;
	
	//Object functions
	void CreateObject(std::string ID, std::string name, std::vector<GLfloat> vertices, float health, int mass, int objID, int objIDTex, fizz::fizz *fizztemp, float inScale);
	RenderObject *GetObj(std::string n);
	void DeleteObj(std::string name);
	void DeleteObjects();
	std::vector<GLfloat> GetCubeVerts() {return cubeVerts;}
	std::vector<GLfloat> GetSphereVerts() {return sphereVerts;}
	std::vector<GLfloat> GetShopVerts() {return shopVerts;}
	std::vector<GLfloat> GetSpaceshipVerts() {return spaceshipVerts;}
	std::vector<GLfloat> GetEnemyVerts() {return enemyVerts;}
	std::vector<GLfloat> GetRocketVerts() {return rocketVerts;}

	//UI Functions
	void RenderGameUI(Shop inShop,float xOffset, float yOffset, std::stack<glm::mat4> inStack);
	RenderObject *CreateUIBox(std::string inID, std::string inName, int inTexID, glm::vec2 pos, glm::vec2 scale);
	std::vector<RenderObject *> RunUI(std::vector<RenderObject *> inList);
	Shop* LoadShop(std::string inID, const char *fname);
	void RenderUIImage(GLuint inImage, std::stack<glm::mat4> inStack, glm::vec3 inTranslate, glm::vec3 inScale);
	void UpdatePlayerMoney(RenderObject* obj);
	void OpenBuyMenu();
	void CloseBuyMenu();	
	void RenderTextNew(const char* inText, std::stack<glm::mat4> inStack, glm::vec3 inTranslate, glm::vec3 inScale);
	const char* IntToConstChar(int input);
	float mouseSensitivity;
	void PauseGame();
	void UnpauseGame();
	bool ReturnQuit(){return pauseQuit;}
	bool ReturnBuyMenu(){return buyMenu;}
	bool ReturnGamePaused() {return gamePaused;}

	//Physics functions
	fizz::fizzypop* physManager;
	fizz::fizzykeys* keyManager;
	fizz::fizzyAIlight* AIManager;

	//Battle Engine functions
	BattleEngine* GetBattleEngine() {return battle_Engine;}
	void Fire();	
	
private:
	
	bool gameOver;
	//Mouse
	float floatMouseX;
	float floatMouseY;
	bool mouseControl;

	//Render private variables
	int cam;	
	glm::vec3 eye;
	glm::vec3 at;
	glm::vec3 up;
	glm::vec3 MCTangent;
	glm::vec3 MCVertex;
	glm::vec3 MCNormal;
	glm::vec3 Ambient;
	glm::vec3 LightColor;
	glm::vec4 lightPos;

	float attConstant;
	float attLinear;
	float attQuadratic;

	#define DEG_TO_RADIAN 0.017453293
	GLfloat r;
	glm::quat camera;
	GLfloat yR;
	float theta;

	//Skybox
	GLuint skybox[5];

	//Model variables
	int meshCount;
	GLuint textures[10];
	GLuint meshObjects[4];
	BitmapLoader *bitmapLoader;
	
	std::vector<GLfloat> cubeVerts;
	std::vector<GLfloat> shopVerts;	
	std::vector<GLfloat> sphereVerts;	
	std::vector<GLfloat> spaceshipVerts;	
	std::vector<GLfloat> enemyVerts;
	std::vector<GLfloat> rocketVerts;
	std::vector<GLfloat> hudVerts;

	GLuint md2VertCount;
	GLuint meshIndexCountCube;
	GLuint meshIndexCountSphere;
	GLuint meshIndexCountSpaceShip;
	GLuint meshIndexCountEnemy;
	GLuint meshIndexCountRocket;
	GLuint meshIndexCountHUD;

	//Shader variables
	GLuint bumpShader;
	GLuint multiLightProgram;
	GLuint simpleProgram;
	GLuint particleShader;
	GLuint depthShader;
	GLuint skyboxProgram;
	GLuint HUDProgram;
	GLuint textureMultipleLights;
	GLuint phongShader;
	GLuint laserFireShader;

	//Text variables
	std::vector<GLuint> labels;	
	TTF_Font * textFont;
		
	//Object variables
	std::vector<RenderObject *> renderList;
	glm::vec3 mouseRotation;	
	
	//UI Stuff
	std::vector<RenderObject *> UIList;
	std::vector<RenderObject *> pauseMenu;
	int mouseX, mouseY;
	GLuint hudImage;
	Shop* shop1;
	Shop* shop2;
	Shop* pauseShop;
	bool pauseQuit;
	Player* player1;
	RenderObject* playerMoneyDisplay;
	RenderObject* shoppingcart;
	bool buyMenu;
	bool gamePaused;
	GLuint healthBar;
	GLuint ammoBar;

	//Battle Engine variables
	BattleEngine * battle_Engine;	
	bool laserFire;	
	bool boundingSet;	
};

