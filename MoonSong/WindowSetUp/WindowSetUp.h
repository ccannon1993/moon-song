/*
*  MoonSong - Conundrum
*  WindowSetUp.h
*  Last Reviewed: 08/05/2014
*/

#pragma once
#include<iostream>
#include <SDL.h>

class WindowSetUp
{
public:
	WindowSetUp(void);
	~WindowSetUp(void);
	SDL_Window *setupRC(SDL_GLContext &context);
};

