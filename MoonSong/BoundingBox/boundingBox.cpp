/*
*  MoonSong - Conundrum
*  BoundingBox.cpp
*  Last Reviewed: 02/04/2014
*  This class is used to set bounding boxes for objects.
*  This class should be used to set the bounding box after the object is
*  created/scaled/rotated so the box stays axis aligned.
*  Min and max will be set so the dimensions of the box can be found.
*  The bounding box can be moved using the object centre position, this will then 
*  set the min and max according to the centre and dimensions of the box.
*  This class can also detect collision against another bounding box.
*/

#include "boundingBox.h"

/*
*  Constructor.
*  Sets ID and name taken in.
*  Sets unrealistic values for min and max.
*/
BoundingBox::BoundingBox(std::string inID, std::string inName)
{
	ID = inID;
	name = inName;	
	min = glm::vec3(99999.99, 99999.99, 99999.99);
	max = glm::vec3(-99999.99, -99999.99, -99999.99);
}

/*
*  Deconstructor
*/
BoundingBox::~BoundingBox()
{

} 

/*
*  Sets min and max points for bounding box.
*  Sets length, width and height, these are usually used for collisions
*  against another type that is not a box.
*  @param - vector - <GLfloat> takes in verts of object from a vector.
*  @param - int - takes in vertex count for object (NOTE, not indices).
*/
void BoundingBox::SetBB(const std::vector<GLfloat> &verts, int vertCount)
{		
	for(int i = 0; i < vertCount; i += 3)
	{
		if( verts[i] < min.x ) min.x = verts[i];
		if( verts[i + 1] < min.y ) min.y = verts[i + 1];
		if( verts[i + 2] < min.z ) min.z = verts[i + 2];
 
		if( verts[i] > max.x ) max.x = verts[i];
		if( verts[i + 1] > max.y ) max.y = verts[i + 1];
		if( verts[i + 2] > max.z ) max.z = verts[i + 2];
	}
	
	//Set Length, Height & Depth based on differences between min/max points.
	length = max.x - min.x;
	height = max.y - min.y;
	depth = max.z - min.z;
}

/*
*  Sets min and max points for bounding box.
*  Sets length, width and height, these are usually for collisions
*  against another type that is not a box.
*  @param - vector - <GLfloat *> takes in verts of object from a vector of pointers
*  due to some objects using pointers.
*  @param - int - takes in vertex count for object (NOTE, not indices).
*/
void BoundingBox::SetPointerBB(const std::vector<GLfloat *> &verts, int vertCount)
{
	for(int i = 0; i < vertCount; i += 3)
	{
		if( *verts[i] < min.x ) min.x = *verts[i];
		if( *verts[i + 1] < min.y ) min.y = *verts[i + 1];
		if( *verts[i + 2] < min.z ) min.z = *verts[i + 2];
 
		if( *verts[i] > max.x ) max.x = *verts[i];
		if( *verts[i + 1] > max.y ) max.y = *verts[i + 1];
		if( *verts[i + 2] > max.z ) max.z = *verts[i + 2];
	}
	
	//Set Length, Height & Depth based on differences between min/max points.
	length = max.x - min.x;
	height = max.y - min.y;
	depth = max.z - min.z;	
}

/*
*  Used to update the centre in respect to the objects centre. 
*  Will set the new min and max based on the bounding boxes centre and
*  the dimensions of the box.
*  @param - glm::vec3 - vector taken in to position the bounding box.
*/
void BoundingBox::UpdatePosition(glm::vec3 position)
{
	centre = position;
	min = glm::vec3(position.x - (length/2), position.y - (height/2), position.z - (depth/2));
	max = glm::vec3(position.x + (length/2), position.y + (height/2), position.z + (depth/2));
}

/*
*  Checks if this bounding box intersects with bounding box taken in.
*  @param - BoundingBox - used for intersection test.
*  @return - boolean - returns true if bounding boxes intersect, false
*  otherwise.
*/
bool BoundingBox::DoBBIntersect(BoundingBox other)
{
	if(max.x <= other.min.x || max.y <= other.min.y || max.z <= other.min.z 
	|| min.x >= other.max.x || min.y >= other.max.y || min.z >= other.max.z)
		return false;
	else
		return true;
}