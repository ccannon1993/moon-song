/*
*  MoonSong - Conundrum
*  BoundingBox.h
*  Last Reviewed: 02/04/2014
*/

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

#ifndef BOUNDINGBOX_H  
#define BOUNDINGBOX_H

class BoundingBox
{
private:
	glm::vec3 min;
	glm::vec3 max;
	glm::vec3 centre;
	float length;
	float height;
	float depth;
	std::string ID;
	std::string name;
public:	
	BoundingBox(std::string inID, std::string inName);
	~BoundingBox();
	glm::vec3 GetMin() { return min; } 
	glm::vec3 GetMax() { return max; } 
	glm::vec3 GetCentre() { return centre; }
	float GetLength() { return length; }
	float GetHeight() { return height; }
	float GetDepth() { return depth; }
	std::string GetID() { return ID; }
	std::string GetName() { return name; }
	void SetBB(const std::vector<GLfloat> &verts, int vertCount);	
	void SetPointerBB(const std::vector<GLfloat *> &verts, int vertCount);
	void UpdatePosition(glm::vec3 position);
	bool DoBBIntersect(BoundingBox other);	
};

#endif
