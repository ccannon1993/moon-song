/*
*  MoonSong - Conundrum
*  fail.cpp
*  Last Reviewed: 08/05/2014
*/

#include "fail.h"
#include <iostream>
#include <iterator>

namespace fizz{

	void fizzyAIlight::update( fizzypop* manager )
	{		
		for(unsigned int i = 0; i < activeUnits.size(); i++)
		{
			glm::quat ori = manager->record[activeUnits[i]]->ori;
			glm::quat look = quat::pointsToQuat( manager->record[unitsToTargets[activeUnits[i]]]->pos - manager->record[activeUnits[i]]->pos, glm::vec3(1.0f, 0.0f, 0.0f) );
			//second parameter is the original orientation, first parameter is the target.
						
			if( 1.0f > quat::angle(look, ori) ){
				manager->record[activeUnits[i]]->ori = quat::interpolate(ori, look, quat::angle(look, ori));
			}else{
				manager->record[activeUnits[i]]->ori = quat::interpolate(ori, look, 1.0f/quat::angle(look, ori));
			}			
			float angle = quat::angle(look, manager->record[activeUnits[i]]->ori);
			if( angle < 20 || angle > 340 ){
				fizz* key = manager->buildFizz();
				key->setVel( glm::vec3(0.01f, 0.0f, 0.0f) );
				manager->record[activeUnits[i]]->apply( key );
			}else{
				
			}
		}		
	}

	void fizzyAIlight::setRotation( float angle ){
		maxAttack = angle;
	}

	int fizzyAIlight::addUnit( int ID, int targetID )
	{
		unitsToTargets.insert( std::make_pair<int, int>(ID, targetID) );		
		activeUnits.push_back( ID );
		return 0;
	}

	fizzyAIlight::fizzyAIlight(void)
	{
	}

	fizzyAIlight::~fizzyAIlight(void)
	{
	}

}