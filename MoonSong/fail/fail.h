/*
*  MoonSong - Conundrum
*  fail.h
*  Last Reviewed: 08/05/2014
*/

#ifndef TEGUKI_SODA_AI
#define TEGUKI_SODA_AI

#include "../fext/fext.h"
#include <vector>
#include <map>

#define ROTATION 0
#define MOVEMENT 1

#define AXIS 0
#define VIEW 1

namespace fizz{

	class fizzyAIlight : public fExt
	{
		friend class fizzypop;
	private:
		float maxAttack;
		std::map<int, int> unitsToTargets;
		std::vector<int> activeUnits;

		std::map<int, fizz*> keysToAlts;
		void update( fizzypop* manager );
	public:
		void setRotation( float angle );
		int addUnit( int unit, int target );
		fizzyAIlight(void);
		~fizzyAIlight(void);
	};

}

#endif
