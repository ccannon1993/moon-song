/*
*  MoonSong - Conundrum
*  Game.h
*  Last Reviewed: 08/05/2014
*/

#pragma once
#include"../RenderEngine/renderEngine.h"
#include"../Quaternion/quaternion.h"
#include"../WindowSetUp/WindowSetUp.h"
#include"../Audiocode/AudioManager.h"

class Game
{
public:
	Game(void);
	~Game(void);
	void Init();
	void Run();
	void Update();
	void Draw(SDL_Window * window);
	void KeyPress();	
private:
	bool running;

	int test1ID;
	int test3ID;
	int test5ID;	
	int test6ID;
	fizz::fizz *test1; // Enemy
	fizz::fizz *test3; // Enemy
	fizz::fizz *test5; // Player
	fizz::fizz *test6; // Another Spaceship/Stationary
	fizz::fizz *test8; // Tracker
	fizz::fizz *test9; // Tracker

	fizz::fizz *rocket1;
	fizz::fizz *rocket2;
	fizz::fizz *rocket3;
	fizz::fizz *rocket4;
	bool laserFire;
	std::stack<glm::mat4> mvStack;
	WindowSetUp setup;

	AudioManager UseFMOD;
	FMOD::Sound *backingTrack;	
	FMOD::Sound *laserShotSound;
	FMOD::Sound *shopSounds;
	FMOD::Channel *channel1;
	FMOD::Channel *channel2;
	FMOD::Channel *channel3;
};

