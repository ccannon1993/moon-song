/*
*  MoonSong - Conundrum
*  Game.cpp
*  Last Reviewed: 08/05/2014
*/

#include "Game.h"

RenderEngine *renderer = new RenderEngine();

fizz::fizz* camera = new fizz::fizz();
float cameraTarget = 7.0f;
bool f4up = true;
GLfloat r = 0.0f;

int lightControl = 0;
float theta = 0.0f;

/*
* Constructor - sets boolean for player laser fire to false on load.
*/
Game::Game(void)
{
	laserFire = false;	
}

/*
* Deconstructor
*/
Game::~Game(void)
{
}

/*
* Load function - Sets sounds requirements, physics requirements including keys.
* Will create all objects in game.
*/
void Game::Init()
{
	//Sound set up.
	_crtBreakAlloc = 137;
	renderer->Init();
	UseFMOD.FMODInit();
	backingTrack = UseFMOD.CreateSound("Music/BackingTrack.mp3");
	laserShotSound = UseFMOD.CreateSound("Music/Laser.wav");
	shopSounds = UseFMOD.StreamSound("Music/ShopSounds.wav");
	channel1 = UseFMOD.PlayLoopedSound(backingTrack);
	channel2 = UseFMOD.PlayLoopedSound(laserShotSound);
	channel3 = UseFMOD.PlayLoopedSound(shopSounds);
	UseFMOD.PauseSound(channel2);
	UseFMOD.PauseSound(channel3);
	channel3->setVolume(0.5);

	//Physics objects.
	fizz::fizz *test2 = renderer->physManager->buildFizz();
	fizz::fizz *test4 = renderer->physManager->buildFizz();
	test6 = renderer->physManager->buildFizz();
	fizz::fizz *test7 = renderer->physManager->buildFizz();//Shop
	test1 = renderer->physManager->buildFizz();
	test3 = renderer->physManager->buildFizz();
	test5 = renderer->physManager->buildFizz();
	test6 = renderer->physManager->buildFizz();
	test8 = renderer->physManager->buildFizz();
	test9 = renderer->physManager->buildFizz();

	test1->setPos(glm::vec3(200.0, 0.0, -50.0));
	test2->setPos(glm::vec3(100000.0, 0.0, 0.0));//planet 1
	test3->setPos(glm::vec3(100.0,0.0, 50.0));
	test4->setPos(glm::vec3(30000.0,-9000.0,-10000.0));//planet 2
	test5->setPos(glm::vec3(0.0,0.0,0.0));
	test6->setPos(glm::vec3(10.0f, 0.0f, 20.0f));
	test7->setPos(glm::vec3(1000.0,1000.0,-1000));
	test7->setOri(quat::eulerToQuat(21, 34, 55));

	rocket1 = renderer->physManager->buildFizz();
	rocket2 = renderer->physManager->buildFizz();
	rocket3 = renderer->physManager->buildFizz();
	rocket4 = renderer->physManager->buildFizz();
	rocket1->setPos(glm::vec3(200.0, 0.0, -50.0));
	rocket2->setPos(glm::vec3(200.0, 0.0, -50.0));
	rocket3->setPos(glm::vec3(200.0, 0.0, -50.0));
	rocket4->setPos(glm::vec3(200.0, 0.0, -50.0));

	test8->setPos( ( glm::normalize( test1->getPos() + renderer->ReturnEye() ) * 1.1f ) - renderer->ReturnEye() );
	test9->setPos( ( glm::normalize( test3->getPos() + renderer->ReturnEye() ) * 1.1f ) - renderer->ReturnEye() );

	//Object creation.
	renderer->CreateObject("SPACESHIP", "Player", renderer->GetSpaceshipVerts(), 1000, 100, 2, 0, test5, 1);	
	renderer->CreateObject("PLANET", "Planet1", renderer->GetSphereVerts(), 100, 10, 1, 1, test2, 1);
	renderer->CreateObject("PLANET", "Planet2", renderer->GetSphereVerts(), 100, 10, 1, 3, test4, 1);
	renderer->CreateObject("PLANET", "Tracker1", renderer->GetSphereVerts(), 100, 10, 1, 6, test9, 0.000003125);
	renderer->CreateObject("PLANET", "Tracker2", renderer->GetSphereVerts(), 100, 10, 1, 6, test8, 0.000003125);
	renderer->CreateObject("ENEMY", "Enemy1", renderer->GetEnemyVerts(), 100, 50, 3, 4, test3, 1);
	renderer->CreateObject("ENEMY", "Enemy2", renderer->GetEnemyVerts(), 100, 50, 3, 4, test1, 1);
	renderer->CreateObject("SPACESHIP", "Ally", renderer->GetSpaceshipVerts(), 100, 100, 2, 4, test6, 1);
	renderer->CreateObject("ROCKET", "Rocket1", renderer->GetRocketVerts(), 100, 10, 5, 1, rocket1, 0.25);
	renderer->CreateObject("ROCKET", "Rocket2", renderer->GetRocketVerts(), 100, 10, 5, 1, rocket2, 0.25);
	renderer->CreateObject("ROCKET", "Rocket3", renderer->GetRocketVerts(), 100, 10, 5, 1, rocket3, 0.25);
	renderer->CreateObject("ROCKET", "Rocket4", renderer->GetRocketVerts(), 100, 10, 5, 1, rocket4, 0.25);

	renderer->GetObj("Rocket1")->MakeInVisible();
	renderer->GetObj("Rocket2")->MakeInVisible();
	renderer->GetObj("Rocket3")->MakeInVisible();
	renderer->GetObj("Rocket4")->MakeInVisible();

	renderer->CreateObject("PLANET", "Shop1", renderer->GetShopVerts(), 100, 10, 4, 5, test7, 10);

	//Physics Manager Set up.
	test1ID = renderer->physManager->attach( test1 );
	renderer->physManager->attach( test2 );
	test3ID = renderer->physManager->attach( test3 );
	renderer->physManager->attach( test4 );
	test5ID = renderer->physManager->attach( test5 );
	test6ID = renderer->physManager->attach( test6 );

	renderer->AIManager->setRotation(1.0f);
	renderer->AIManager->addUnit(test1ID, test5ID);
	renderer->AIManager->addUnit(test3ID, test5ID);

	renderer->physManager->attach( renderer->AIManager );

	//Keyboard controls set up.
	fizz::fizz* key = renderer->physManager->buildFizz();	
	key->setVel( test5->getVel() * -0.1f );
	renderer->keyManager->setKey( SDL_SCANCODE_C, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();		
	key->setVel( glm::vec3(0.0125f, 0.0f, 0.0f) );
	renderer->keyManager->setKey( SDL_SCANCODE_SPACE, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();	
	key->setVel( glm::vec3(-0.0125f, 0.0f, 0.0f) );
	renderer->keyManager->setKey( SDL_SCANCODE_X, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();	
	key->setVel( glm::vec3(0.0f, 0.0125f, 0.0f) );
	renderer->keyManager->setKey( SDL_SCANCODE_I, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();	
	key->setVel( glm::vec3(0.0f, -0.0125f, 0.0f) );
	renderer->keyManager->setKey( SDL_SCANCODE_K, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();	
	key->setVel( glm::vec3(0.0f, 0.0f, 0.0125f) );
	renderer->keyManager->setKey( SDL_SCANCODE_L, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();	
	key->setVel( glm::vec3(0.0f, 0.0f, -0.0125f) );
	renderer->keyManager->setKey( SDL_SCANCODE_J, test5ID, key->copy() );

	key = renderer->physManager->buildFizz();
	key->setAngVel( quat::eulerToQuat( 0.01f, 0, 0 ) );
	renderer->keyManager->setKey( SDL_SCANCODE_Q, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();
	key->setAngVel( quat::eulerToQuat( -0.01f, 0, 0 ) );
	renderer->keyManager->setKey( SDL_SCANCODE_E, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();
	key->setAngVel( quat::eulerToQuat( 0, 0.005f, 0 ) );
	renderer->keyManager->setKey( SDL_SCANCODE_D, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();
	key->setAngVel( quat::eulerToQuat( 0, -0.005f, 0 ) );
	renderer->keyManager->setKey( SDL_SCANCODE_A, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();
	key->setAngVel( quat::eulerToQuat( 0, 0, 0.005f ) );
	renderer->keyManager->setKey( SDL_SCANCODE_W, test5ID, key->copy() );
	key = renderer->physManager->buildFizz();
	key->setAngVel( quat::eulerToQuat( 0, 0, -0.01f ) );
	renderer->keyManager->setKey( SDL_SCANCODE_S, test5ID, key->copy() );

	renderer->physManager->attach( renderer->keyManager );
}

/*
* Keys that can be used during game play. 
* This function will also check certain variables to see if updates are required.
*/
void Game::KeyPress()
{
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	laserFire = false;

	if(renderer->GetBattleEngine()->GetShop1() == true)
	{
		UseFMOD.PauseSound(channel1);
		UseFMOD.PlaySoundAfterPause(channel3);
		renderer->OpenBuyMenu();
	}

	if ( keys[SDL_SCANCODE_TAB] ) {
		if(renderer->GetBattleEngine()->GetShop1() == true)
		{
			glm::vec3 exitShop; //Used to set player coordinates so no longer intersecting shop when closes shop menu
			exitShop.x = renderer->GetObj("Player")->GetPosition().x - renderer->GetObj("Player")->GetPosition().x + renderer->GetObj("Shop1")->GetPosition().x + 470.0;
			exitShop.y = renderer->GetObj("Player")->GetPosition().y - renderer->GetObj("Player")->GetPosition().y + renderer->GetObj("Shop1")->GetPosition().y + 470.0;
			exitShop.z = renderer->GetObj("Player")->GetPosition().z - renderer->GetObj("Player")->GetPosition().z + renderer->GetObj("Shop1")->GetPosition().z;// + 470.0;
			renderer->physManager->detach(test5ID);	
			renderer->GetObj("Player")->SetPosition(exitShop);
			renderer->physManager->attach(test5);
			renderer->CloseBuyMenu();
			UseFMOD.PauseSound(channel3);
			UseFMOD.PlaySoundAfterPause(channel1);
		}		
	}	

	if ( keys[SDL_SCANCODE_ESCAPE] ) renderer->PauseGame();

	if ( keys[SDL_SCANCODE_F4] ){
		if( f4up ){
			f4up = false;
			if( cameraTarget == 7.0f )
				cameraTarget = -1.0f;
			else
				cameraTarget = 7.0f;
		}
	}
	else	
		f4up = true;

	renderer->camDist += (cameraTarget - renderer->camDist)*0.1f;

	if ( keys[SDL_SCANCODE_LEFT] ) r -= 1.0f;
	if ( keys[SDL_SCANCODE_RIGHT] ) r += 1.0f;	

	if ( keys[SDL_SCANCODE_F] )
	{		
		renderer->Fire();
		UseFMOD.PlaySoundAfterPause(channel2);
	} 	
}

/*
* Updates the game. Will quit the game if pressed on the in game menu.
* Calls the key press function to check if any action is required.
* Calls the update function for the render engine.
*/
void Game::Update(void) 
{
	if(renderer->ReturnQuit()){running = false;}
	KeyPress();
	renderer->Update(r);
}

/*
* Detaches and reattachs the physic managers for each object so they can be dealt with 
* inside the render engine draw function.
* Will call the draw function in the render engine. 
* @param - SDL_Window - the window to be drawn too.
*/
void Game::Draw(SDL_Window * window) 
{	
	test8->setPos( ( glm::normalize( test1->getPos() + renderer->ReturnEye() ) * 1.1f ) -renderer->ReturnEye() );
	test9->setPos( ( glm::normalize( test3->getPos() + renderer->ReturnEye() ) * 1.1f ) - renderer->ReturnEye() );
	renderer->physManager->detach(test1ID);
	renderer->physManager->detach(test3ID);
	renderer->physManager->detach(test5ID);
	renderer->physManager->detach(test6ID);
	renderer->Draw(window, mvStack);	
	renderer->physManager->attach(test1);
	renderer->physManager->attach(test3);
	renderer->physManager->attach(test5);
	renderer->physManager->attach(test6);
	
}

/*
* The game run loop.
*/
void Game::Run()
{
	SDL_Window * hWindow; // window handle
	SDL_GLContext glContext; // OpenGL context handle
	hWindow = setup.setupRC(glContext);
	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << std::endl;
		exit (1);
	}
	std::cout << glGetString(GL_VERSION) << std::endl;

	Init();

	running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if(sdlEvent.type == SDL_KEYUP)
			{
				switch( sdlEvent.key.keysym.sym ){
                 case SDLK_f:
					 UseFMOD.PauseSound(channel2);
					 break;
				 default:
					break;
				}
			}
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		Update();
		Draw(hWindow); // call the draw function
		UseFMOD.FMODUpdate();
	}
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(hWindow);
	SDL_Quit();
};