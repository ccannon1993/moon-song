/*
*  MoonSong - Conundrum
*  userInterface.h
*  Last Reviewed: 08/05/2014
*/

#pragma once
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL\glew.h>

class UserInterface
{
public:
	UserInterface(void);
	UserInterface(std::string objID, int objIDTex, glm::vec2 pos, glm::vec2 inScale);
	~UserInterface(void);

	glm::vec2 GetUIPosition() {return UIpos;}
	glm::vec2 GetUIScale() {return UIscale;}
	void SetUITexture(GLuint inTexture);
	GLuint ReturnUITexure(){return UITexture;}
	void Highlighted(){mouseOver = true;}
	void NotHighlighted(){mouseOver = false;}
	bool ReturnMouseOver(){return mouseOver;}

private:
	std::string ID;
	glm::vec2 UIpos;
	glm::vec2 UIscale;
	GLuint UITexture;
	bool mouseOver;
	bool visible;

};

