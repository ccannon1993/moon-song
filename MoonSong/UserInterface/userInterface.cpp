/*
*  MoonSong - Conundrum
*  UserInterface.cpp
*  Last Reviewed: 08/05/2014
*/

#include "userInterface.h"

/*
* Constructor.
*/
UserInterface::UserInterface(void)
{
}

/*
* Parameter Constructor.
* @param - std::string - object ID.
* @param - int - texture ID.
* @param - glm::vec2 - position of the interface.
* @param - glm::vec2 - scale for the interface.
*/
UserInterface::UserInterface(std::string objID, int objIDTex, glm::vec2 pos, glm::vec2 inScale)
{
	ID = objID;
	//vertexData = NULL;
	UIpos = pos;
	UIscale = inScale;
	visible = true;
	mouseOver = false;
}

/*
* Deconstructor.
*/
UserInterface::~UserInterface(void)
{
}

/*
* Sets texture for the user interface.
* @param - GLuint - texture to be set.
*/
void UserInterface::SetUITexture(GLuint inTexture)
{
	UITexture = inTexture;
}