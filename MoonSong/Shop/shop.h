/*
*  MoonSong - Conundrum
*  shop.h
*  Last Reviewed: 08/05/2014
*/

#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "../RenderObject/renderObject.h"
#include "../BitmapLoader/BitmapLoader.h"
#include <SDL_ttf.h>
#include "../Player/player.h"
#include"../AudioCode/AudioManager.h"

class Shop
{
public:
	Shop(void);
	Shop(std::string ID, const char *fname, const char *weaponfname,const char *commodityfname);
	~Shop(void);
	void Init();
	void RunPause();
	void OpenShop();
	void CloseShop();
	bool ReturnOpen(){return shopOpen;}
	int BuyPrimary();	
	int GetHighlightedPrice(){return highlightedPrice;}
	Player* BuyItem(int cost,int count, std::string item, Player* tempPlayer);
	void SellItem(int cost,int count, std::string item, Player* tempPlayer);
	int ReturnMoney() {return money;}
	int ReturnNoPrimaryWeapons() {return noPrimaryWeapons;}	
	std::vector<RenderObject *> ReturnUIList(){return activeList;}
	std::vector<RenderObject *> ReturnWeaponsList(){return weaponsList;}
	std::vector<RenderObject *> ReturnCommodityList(){return commodityList;}
	bool ReturnQuit(){return quit;}
	Player* ReturnPlayer(){return myPlayer;}
	void UpdatePlayer(Player* inPlayer);
	std::vector<RenderObject *> LoadUIList(const char *fname);
	RenderObject* CreateUIBox(std::string inID, std::string inName, int inTexID, glm::vec2 pos, glm::vec2 inScale);
	GLuint TextToTexture(const char * str, SDL_Color colour/*, TTF_Font *font, SDL_Color colour, GLuint &w,GLuint &h*/);
	TTF_Font* LoadFont(const char* fname);
	std::vector<RenderObject *> RunUI();
	void UpdatePlayerMoney(int playerMoney);
	int ReturnPlayerMoney(){return playerMoney;}
	void UpdateTexture(RenderObject* inObj, SDL_Color inCol);
	GLuint ReturnUIImage(int i);
	int ReturnUIImageNo(){return UIImage;}
	int ReturnPrice(const char* inItem);
	bool ReturnPlayerInShop(){return playerInShop;}
	GLuint ReturnShopIcon(){return shoppingcart;}
	void SetShopIcon(bool in);

private:
	TTF_Font * textFont;
	int mouseX, mouseY;
	std::string ID;
	int money;
	int noPrimaryWeapons;
	int noSecondaryWeapons;
	int primaryCost;
	int secondaryCost;

	Player* myPlayer;
	//Lists
	std::vector<RenderObject *> activeList;
	std::vector<RenderObject *> UIList;
	std::vector<RenderObject *> weaponsList;
	std::vector<RenderObject *> commodityList;

	std::vector<GLuint> UI;
	const char *fileName;
	const char *weaponfileName;
	const char *commodityfileName;
	int playerMoney;
	BitmapLoader *bitmapLoader;
	GLuint itemImages[6];
	int UIImage;
	GLuint shoppingcart;
	bool playerInShop;
	bool shopOpen;

	int copperPrice;
	int goldPrice;
	int platinumPrice;
	int highlightedPrice;

	bool mouseUp;
	bool rMouseUp;
	
	bool quit;
	SDL_Event mEvent;

	AudioManager FMODSounds;
	FMOD::Sound *clicks;
	FMOD::Sound *cash;
};

