/*
*  MoonSong - Conundrum
*  shop.cpp
*  Last Reviewed: 08/05/2014
*/

#include "shop.h"

/* An SDL_Event struct */
SDL_Event sdlEvent;
static const SDL_Colour selected = {100, 225, 100 };
static const SDL_Colour notSelected = { 50,50, 50 };

/*
* Constructor.
*/
Shop::Shop(void)
{
}

/*
* Parameter Constructor.
* @param - std::string - ID of object.
* @param - const char - name of file for object.
* @param - const char - name of weapon file for object.
* @param - const char - name of commodity file for object.
*/
Shop::Shop(std::string inID, const char *fname,const char *weaponfname,const char *commodityfname)
{
	ID = inID;
	money = 100;
	noPrimaryWeapons = 5;
	noSecondaryWeapons = 5;
	primaryCost = 25;
	secondaryCost = 40;
	fileName = fname;
	commodityfileName = commodityfname;
	weaponfileName = weaponfname;
	bitmapLoader = new BitmapLoader();
	activeList = UIList;
}

/*
* Deconstructor.
*/
Shop::~Shop(void)
{
}

/*
* Returns text to be displayed on screen.
* @param - const char - file to be opened for text font.
* @return - TTF_Font - the font style to be returned.
*/
TTF_Font* Shop::LoadFont(const char* fname)
{
	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		std::cout << "TTF failed to initialise." << std::endl;
	textFont = TTF_OpenFont(fname, 24);
	if (textFont == NULL)
		std::cout << "Failed to open font." << std::endl;
	return textFont;
}

/*
* Load function for shop, sets variables and images that are used.
*/
void Shop::Init()
{
	copperPrice = 1;
	goldPrice = 2;
	platinumPrice = 3;
	highlightedPrice = 0;
	
	textFont = LoadFont("UI/homespun.ttf");
	FMODSounds.FMODInit();
	clicks = FMODSounds.CreateSound("Music/ShopClicks.wav");
	cash = FMODSounds.StreamSound("Music/CashSound.wav");
	UIList = LoadUIList(fileName);
	activeList = UIList;
	weaponsList = LoadUIList(weaponfileName);
	commodityList = LoadUIList(commodityfileName);
	
	itemImages[0] = bitmapLoader->loadBitmap("UI/primaryAmmoExtended.bmp");//http://almightycookie.blogspot.co.uk/2010/01/crate-ideas.html edited
	itemImages[1] = bitmapLoader->loadBitmap("UI/secondaryAmmoExtended.bmp");//http://almightycookie.blogspot.co.uk/2010/01/crate-ideas.html edited
	itemImages[2] = bitmapLoader->loadBitmap("UI/repair.bmp");
	itemImages[4] = bitmapLoader->loadBitmap("UI/hangardoor.bmp");
	itemImages[5] = bitmapLoader->loadBitmap("UI/commoditiesExtended.bmp");	

	playerInShop = true;
	shopOpen = false;
	quit = false;
	mouseUp = true;
	rMouseUp = true;

	myPlayer = new Player(100);
	myPlayer->LoadStats("PlayerSaveFiles/Inventory.txt");  
}

/*
* Returns interface image.
* @param - int - number used to know which image to get.
* @return - GLuint - image to be returned.
*/
GLuint Shop::ReturnUIImage(int i)
{
	if(i < 0 || i > 6 ){i = 1;}
	return itemImages[i];
}

/*
* Buys primary ammo for the player.
* @param - int - the amount of ammo bought.
*/
int Shop::BuyPrimary()
{
	if(playerMoney > primaryCost && noPrimaryWeapons > 0){
		playerMoney -= primaryCost;
		noPrimaryWeapons--;
	} 
	return playerMoney;
}

/*
* Sets text to a texture.
* @param - const char - the text to be displayed.
* @param - SDL_Color - the colour of the text.
* @return - GLuint - the texture to be returned.
*/
GLuint Shop::TextToTexture(const char * str, SDL_Color colour)
{
	TTF_Font *font = textFont;	
	SDL_Color bg = { 0, 0, 0 };

	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(font,str,colour);//font needs loaded properly 

	if (stringImage == NULL)		
		std::cout << "String surface not created." << std::endl;

	GLuint w = stringImage->w;
	GLuint h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
		else
			format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
		else
			format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
		format, GL_UNSIGNED_BYTE, stringImage->pixels);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);
	return texture;
}

/*
* Updates the texture for the shop image.
* @param - RenderObject - the object to be updated.
* @param - SDL_Color - the colour to be set.
*/
void Shop::UpdateTexture(RenderObject* inObj, SDL_Color inCol)
{
	std::string inName = inObj->GetName();
	inObj->SetUITexture(TextToTexture(inName.c_str(),inCol));
}

/*
* Creates user interface for the object.
* @param - std::string - the ID of the object.
* @param - std::string - the name of the object.
* @param - int- the texture ID of the object.
* @param - glm::vec2 - the position of the object.
* @param - glm::vec2 - the scale of the object.
* @return - RenderObject - the object to be returned.
*/
RenderObject* Shop::CreateUIBox(std::string inID, std::string inName, int inTexID, glm::vec2 pos, glm::vec2 inScale)
{
	SDL_Color green = { 0, 255, 0 };
	RenderObject * obj = new RenderObject(inID, inName, inTexID, pos, inScale);
	obj->SetUITexture(TextToTexture(inName.c_str(), green));
	return obj;	
}

/*
* Loads the user interface list.
* @param - const char - the name of the file list.
* @return - std::vector<RenderObject> - the list of objects to be returned.
*/
std::vector<RenderObject *> Shop::LoadUIList(const char *fname)
{
	SDL_Color green = { 0, 255, 0 };
	float xOffset = -0.95;
	float yOffset = 0.85;
	std::vector<RenderObject *> temp;
	float xScale = 0.15;
	float yScale = 0.1;
	std::string line; 
	std::ifstream myfile (fname);
	std::vector<RenderObject *> tempList;
	int i=0;
	if (myfile.is_open())
	{
		while ( getline (myfile,line) )
		{
			std::stringstream stream(line);
			float length = line.size();
			temp.push_back(CreateUIBox("UIOBJECT",line,i,glm::vec2((xOffset)+(xScale*(length/5)), (yOffset)+i*(-yScale*2)), glm::vec2(xScale*(length/5), yScale)));
			UI.push_back(TextToTexture(line.c_str(), green));
			i++;
		}
	}
	return temp;
}

/*
* Updates the player object from the object taken in.
* @param - Player - the new player to be set against the existing player.
*/
void Shop::UpdatePlayer(Player* inPlayer)
{
	myPlayer = inPlayer;
}

/*
* Returns the price of items.
* @param - const char - the name of the item.
* @return - int - the price of the item to be returned. 
*/
int Shop::ReturnPrice(const char* inItem)
{
	if(inItem == "Copper"){return copperPrice;}
	if(inItem == "Gold"){return goldPrice;}
	if(inItem == "Platinum"){return platinumPrice;}
}

/*
* Buys an item from the shop. Will display error message if you do not have enough money.
* @param - int - the cost of the item.
* @param - int - the amount of the items.
* @param - Player - the player to be used to buy the item.
* @return - Player - the updated player after buying an item.
*/
Player* Shop::BuyItem(int cost,int count, std::string item, Player* tempPlayer)
{
	if(cost > myPlayer->GetPlayerMoney()){
		std::cout << "Not enough cash! " << std::endl;
	}
	else{
		myPlayer->UpdateMoney(myPlayer->GetPlayerMoney() - (cost*count));
		myPlayer->AddItem(item, count);
		std::cout << "You have " << myPlayer->GetItem(item)<< item << std::endl;
	}
	
	FMODSounds.PlaySingleSound(cash);
	return myPlayer;
}

/*
* Opens shop.
*/
void Shop::OpenShop()
{
	shopOpen = true;
}

/*
* Closes Shop
*/
void Shop::CloseShop()
{
	shopOpen = false;
}

/*
* Sells an item to the shop.
* @param - the cost of the item.
* @param - the amount of the item.
* @param - the player to be updated.
*/
void Shop::SellItem(int cost,int count, std::string item, Player* tempPlayer)
{
	if(myPlayer->GetItem(item) <= 0){
		std::cout << "You have no " << item << " left to sell" << std::endl;
	}
	else
	{
		myPlayer->UpdateMoney(myPlayer->GetPlayerMoney() + (cost*count));
		myPlayer->RemoveItem(item, count);
		std::cout << "You have " << myPlayer->GetItem(item) << item << std::endl;
	}
}

/*
* Pauses the game when the shop is running.
*/
void Shop::RunPause()
{
	
	SDL_ShowCursor(1);
	SDL_GetMouseState(&mouseX,&mouseY);
	float mouseXfloat = mouseX;
	mouseXfloat = ((mouseXfloat/800)*2)-1;
	float mouseYfloat = mouseY;
	mouseYfloat = ((mouseYfloat/600)*2)-1;
	float leftSide;
	float rightSide;
	float bottom;
	float top;
	float size;
	float xOffset = 0.7;
	float yOffset = -0.4;

	for(int i=0; i<activeList.size();i++)
	{
		RenderObject  * obj = activeList[i];
		leftSide = obj->GetUIPosition()[0] - ((obj->GetUIScale()[0]))+xOffset;
		rightSide = (obj->GetUIPosition()[0]  + obj->GetUIScale()[0])+xOffset;
		top = obj->GetUIPosition()[1] + ((obj->GetUIScale()[1]))+yOffset;
		bottom = (obj->GetUIPosition()[1] - obj->GetUIScale()[1])+yOffset;
		if(mouseXfloat > leftSide && mouseXfloat <  rightSide && -mouseYfloat > bottom && -mouseYfloat < top){

			UpdateTexture(obj,selected);

			if(SDL_GetMouseState(NULL, NULL)&SDL_BUTTON(1))
			{
				if(mouseUp){
				if(obj->GetName() == "Options"){activeList = weaponsList;}
				if(obj->GetName() == "Save"){activeList = commodityList;}
				if(obj->GetName() == "Back"){activeList = UIList;}
				if(obj->GetName() == "Resume"){shopOpen = false;}
				if(obj->GetName() == "Quit"){quit = true;}
				if(obj->GetName() == "Save1"){myPlayer->SaveGame("PlayerSaveFiles/save1.txt");}
				if(obj->GetName() == "Save2"){myPlayer->SaveGame("PlayerSaveFiles/save2.txt");} 
				if(obj->GetName() == "Save3"){myPlayer->SaveGame("PlayerSaveFiles/save3.txt");}
		
				mouseUp = false;
				}
				SDL_PumpEvents();
			
			}else mouseUp = true;
		} else UpdateTexture(obj,notSelected);
	}

}

/*
* Runs the user interface for the shop
* @return - std::vector<RenderObject> - the list of objects to be returned.
*/
std::vector<RenderObject *> Shop::RunUI()
{


	SDL_ShowCursor(1);
	SDL_GetMouseState(&mouseX,&mouseY);
	float mouseXfloat = mouseX;
	mouseXfloat = ((mouseXfloat/800)*2)-1;
	float mouseYfloat = mouseY;
	mouseYfloat = ((mouseYfloat/600)*2)-1;
	float leftSide;
	float rightSide;
	float bottom;
	float top;
	float size;

	for(int i=0; i<activeList.size();i++)
	{
		RenderObject  * obj = activeList[i];
		leftSide = obj->GetUIPosition()[0] - ((obj->GetUIScale()[0]));
		rightSide = (obj->GetUIPosition()[0]  + obj->GetUIScale()[0]);
		top = obj->GetUIPosition()[1] + ((obj->GetUIScale()[1]));
		bottom = (obj->GetUIPosition()[1] - obj->GetUIScale()[1]);
		if(mouseXfloat > leftSide && mouseXfloat <  rightSide && -mouseYfloat > bottom && -mouseYfloat < top){

			UpdateTexture(obj,selected);

			if(activeList == UIList)
			{
				if(obj->GetName() == "Buy Ammo"){UIImage = 0;}
				if(obj->GetName() == "Secondary Ammo"){UIImage = 1;}
				if(obj->GetName() == "Repair"){UIImage = 2;}				
				if(obj->GetName() == "Weapons"){UIImage = 4;}
				if(obj->GetName() == "Commodities"){UIImage = 5;}
				if(obj->GetName() == "Gold"){highlightedPrice = 10;}
				if(obj->GetName() == "Platinum"){highlightedPrice = 20;}
				if(obj->GetName() == "Copper"){highlightedPrice = 5;}
			}else highlightedPrice = 0;



			if(SDL_GetMouseState(NULL, NULL)&SDL_BUTTON(1))
			{
				if(mouseUp){
				if(obj->GetName() == "Buy Ammo"){myPlayer = BuyItem(/*get shops rate*/15,1,"Pammo",myPlayer);}
				if(obj->GetName() == "Secondary Ammo"){myPlayer = BuyItem(/*get shops rate*/30,1,"Sammo",myPlayer);}
				if(obj->GetName() == "Repair"){myPlayer = BuyItem(/*get shops rate*/50,1,"repair",myPlayer);}
				if(obj->GetName() == "Back"){activeList = UIList;}
				if(obj->GetName() == "Weapons"){activeList = weaponsList;} 
				if(obj->GetName() == "Commodities"){activeList = commodityList;}
				if(obj->GetName() == "Gold"){myPlayer = BuyItem(/*get shops rate*/10,1,"Gold",myPlayer);}
				if(obj->GetName() == "Platinum"){myPlayer = BuyItem(/*get shops rate*/20,1,"Platinum",myPlayer);}
				if(obj->GetName() == "Copper"){myPlayer = BuyItem(/*get shops rate*/5,1,"Copper",myPlayer);}
				if(obj->GetName() == "Heavy Laser"){myPlayer = BuyItem(/*get shops rate*/75,1,"Heavy Laser",myPlayer);}
				if(obj->GetName() == "Medium Laser"){myPlayer = BuyItem(/*get shops rate*/50,1,"Medium Laser",myPlayer);}
				if(obj->GetName() == "Light Laser"){myPlayer = BuyItem(/*get shops rate*/10,1,"Light Laser",myPlayer);}

				SDL_PumpEvents();
				mouseUp = false;
				}
			} else mouseUp = true;

			if(SDL_GetMouseState(NULL, NULL)&SDL_BUTTON(3))
			{
				if(rMouseUp){
				if(obj->GetName() == "Gold"){SellItem(/*get shops rate*/10,1,"Gold",myPlayer);}
				if(obj->GetName() == "Platinum"){SellItem(/*get shops rate*/20,1,"Platinum",myPlayer);}
				if(obj->GetName() == "Copper"){SellItem(/*get shops rate*/5,1,"Copper",myPlayer);}
				rMouseUp = false;
				}
			} else rMouseUp = true;
		}
		else UpdateTexture(obj, notSelected);

	}
	return UIList;
}

/*
* Updates the players money.
* @param - int - the amount of money to be set against the player.
*/
void Shop::UpdatePlayerMoney(int inPlayerMoney)
{
	playerMoney = inPlayerMoney;
}

/*
* Sets the shop icon.
* @param - boolean - true or false to know if icon to be set.
*/
void Shop::SetShopIcon(bool in)
{
	playerInShop = in;
}