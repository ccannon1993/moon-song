#pragma once

/*
*  MoonSong - Conundrum
*  renderObject.h
*  Last Reviewed: 08/05/2014
*/

#include <GL\glew.h>
#include <SDL.h>
#include <iostream>
#include <fstream>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include <string>
#include <vector>
#include "../fizzypop/fizzypop.h"
#include <ctime>

class RenderObject
{
public:
	RenderObject(void);	
	~RenderObject(void);	
	RenderObject(std::string ID, std::string name, std::vector<GLfloat> vertices, float health, int mass, int objID, int objIDTex, fizz::fizz *fizztemp, float inScale);
	RenderObject(std::string objID, std::string objName, int objIDTex, glm::vec2 pos, glm::vec2 inScale);
	//Getters
	int GetMeshID() {return objectMeshID;}
	int GetTextureID() {return objectTextureID;}
	GLuint GetShader() {return shader;}
	float GetScale() {return scale;}
	glm::vec3 GetPosition() {return fizzObj->getPos();}
	glm::quat GetRotation() {return fizzObj->getOri();}
	glm::vec3 GetVelocity() {return fizzObj->getVel();}	
	std::string GetID() {return ID;}
	std::string GetName() {return name;}
	std::vector<GLfloat> GetVertexData() {return vertexData;}
	float GetHealth() {return health;}
	glm::vec2 GetUIPosition() {return UIpos;}
	glm::vec2 GetUIScale() {return UIscale;}
	fizz::fizz* GetFizz(){return fizzObj;}
	int GetMass() {return mass;}		
	bool IsVisible() {return visible;}
	glm::vec3 GetFireColour() {return fireColour;}
	float GetFireSize() {return fireSize;}
	float GetFireDamage() {return fireDamage;}
	bool GetLaserDisabled() {return laserDisabled;}
	clock_t GetLaserStart() {return lStart;}
	clock_t GetLaserFinish() {return lFinish;}
	clock_t GetLaserFireStart() {return laserStart;}
	clock_t GetLaserFire() {return laserFire;}
	clock_t GetLaserFireFinish() {return laserFinish;}

	//Setters
	void SetPosition(glm::vec3 pos) {fizzObj->setPos(pos);}
	void SetVelocity(glm::vec3 vel) {fizzObj->setVel(vel);}	
	void SetAngle(glm::quat ang) {fizzObj->setAngVel(ang);}
	void SetHealth(float inHealth);
	void SetTexture(int inTexture);
	void MakeVisible() {visible = true;}
	void MakeInVisible() {visible = false;}
	void SetFireColour(glm::vec3 newFire) {fireColour = newFire;}
	void SetFire(float fire) {fireSize = fire;}
	void SetFireDamage(float fireD) {fireDamage = fireD;}
	void SetLaserDisabled(bool laser) {laserDisabled = laser;}
	void SetLaserStart(clock_t s) {lStart = s;}
	void SetLaserFinish(clock_t f) {lFinish = f;}
	void SetLaserFireStart(clock_t s) {laserStart = s;}
	void SetLaserFire(clock_t fire) {laserFire = fire;}
	void SetLaserFireFinish(clock_t f) {laserFinish = f;}

	void ApplyDefaultShader(GLuint inShader);
	void UpdateVertices(glm::mat4 modelview);
	
	//UI functions
	void SetUITexture(GLuint inTexture);
	GLuint ReturnUITexure(){return UITexture;}
	void Highlighted(){mouseOver = true;}
	void NotHighlighted(){mouseOver = false;}
	bool ReturnMouseOver(){return mouseOver;}

private:
	std::string ID;
	std::string name;
	std::vector<GLfloat> vertexData;
	float health;	
	int mass;
	int objectMeshID; //Will tell render function what mesh to draw
	int objectTextureID; //Will tell the render function what texture to use	
	fizz::fizz *fizzObj;
	float scale;
	bool visible;
	GLuint shader;
	glm::vec3 fireColour;
	float fireSize;
	float fireDamage;
	bool laserDisabled;
	clock_t lStart;
	clock_t lFinish;
	clock_t laserStart;
	clock_t laserFire;
	clock_t laserFinish;
	glm::vec2 UIpos;
	glm::vec2 UIscale;
	GLuint UITexture;
	bool mouseOver;
};

