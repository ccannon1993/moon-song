/*
*  MoonSong - Conundrum
*  renderObject.cpp
*  Last Reviewed: 08/05/2014
*  Holds all information about renderable objects.
*/

#include "renderObject.h"
#include <cstdlib>

/*
*  Default Constructor.
*/
RenderObject::RenderObject()
{

}

/*
*  Deconstructor.
*/
RenderObject::~RenderObject(void)
{

}

/*
*  Parameter Constructor.
*  @param - std::string - ID of object, @param - std::string - name of object, @param - std::vector - object vertices.
*  @param - float - health of object, @param - int - mass of object, @param - int - meshID of object. 
*  @param - int - textureID of object, @param - fizz::fizz - physics of object, @param - float - scale of object.
*/
RenderObject::RenderObject(std::string objID, std::string objName, std::vector<GLfloat> objVertices, float objHealth, int objMass, int meshID, int objIDTex, fizz::fizz *fizztemp, float inScale)
{			
	ID = objID;
	name = objName;
	vertexData = objVertices;
	health = objHealth;
	mass = objMass;	
	objectMeshID = meshID;
	objectTextureID = objIDTex;
	fizzObj = fizztemp;
	scale = inScale;
	visible = true;
	shader = NULL;
	if(ID == "SPACESHIP")
	{
		fireColour = glm::vec3(255.0f, 0.0f, 0.0f);
		fireSize = 2.0;
		fireDamage = 0.5;
		laserDisabled = false;
	}
	if(ID == "ENEMY")
	{
		fireColour = glm::vec3(0.0f, 255.0f, 0.0f);
		fireSize = 3.0;
		fireDamage = 0.5;
		laserDisabled = false;
		clock_t laserStart = clock();
		clock_t laserFire = rand() % 3000 + (clock() + 3000);
		clock_t laserFinish = rand() % 3000 + (laserFire + 1000);
	}
}

/*
*  Second Parameter Constructor.
*  @param - std::string - ID of object, @param - std::string - name of object.
*  @param - int - textureID of object, @param - glm::vec2 - position of object, @param - glm::vec2 - scale of object.
*/
RenderObject::RenderObject(std::string objID, std::string objName, int objIDTex, glm::vec2 pos, glm::vec2 inScale)
{
	ID = objID;
	name = objName;	
	mass = NULL;	
	objectMeshID = NULL;
	objectTextureID = objIDTex;
	UIpos = pos;
	UIscale = inScale;
	visible = true;
	shader = NULL;
	mouseOver = false;
}

/*
*  Sets health for object.
*  @param - float - new health(already calculated).
*/
void RenderObject::SetHealth(float inHealth)
{
	health = inHealth;
}

/*
*  Sets shader for object.
*  @param - GLuint - new shader.
*/
void RenderObject::ApplyDefaultShader(GLuint inShader)
{
	shader = inShader;
}

/*
*  Updates vertices after they have been scaled and rotated.
*  @param - glm::mat4 - matrix to update vertices.
*/
void RenderObject::UpdateVertices(glm::mat4 modelview)
{	
	
	for(int i = 0; i < vertexData.size(); i += 3)
	{
		glm::vec4 tmpVec = modelview * glm::vec4(vertexData[i], vertexData[i+1], vertexData[i+2], 1.0);		
		vertexData[i] = tmpVec.x;
		vertexData[i+1] = tmpVec.y;
		vertexData[i+2] = tmpVec.z;		
	}
}

/*
*  Sets UI Texture.
*  @param - GLuint - new texture.
*/
void RenderObject::SetUITexture(GLuint inTexture)
{
	UITexture = inTexture;
}

/*
*  Sets Object Texture.
*  @param - GLuint - new texture.
*/
void RenderObject::SetTexture(int inTexture)
{
	objectTextureID = inTexture;
}