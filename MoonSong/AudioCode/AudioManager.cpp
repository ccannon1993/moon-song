/*
*  MoonSong - Conundrum
*  AudioManager.cpp
*  Last Reviewed: 08/05/2014
*/
#pragma region includes, constructor & deconstructor
#include "AudioManager.h"

/*
* Constructor
*/
AudioManager::AudioManager(void)
{
}

/*
* Deconstructor
*/
AudioManager::~AudioManager(void)
{
}
#pragma endregion

#pragma region Error Checking Function & FMOD Initialisers. 

/*
* Function used to get any errors that the FMOD functions can return.
* @param FMOD_RESULT result - this param is used to get the resulting errors from
*							  functions in order to be able to output them to the user.
*/
void AudioManager::FMODErrorCheck(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		std::cout << "FMOD error! (" << result << ") " << FMOD_ErrorString(result) << std::endl;
		exit(-1);
	}
}

/*
* Create a System object and initialize.
*/
void AudioManager::CreateSystem()
{
	result = FMOD::System_Create(&system);
	FMODErrorCheck(result);
}

/*
* Checks the version of FMOD that is required for the program and 
* if the version being used is an old version then the error message 
* displays the version being used and also the version required. 
*/
void AudioManager::FMODCheckVersion()
{
	// Check version
	result = system->getVersion(&version);
	FMODErrorCheck(result);

	if (version < FMOD_VERSION)
	{
		std::cout << "Error! You are using an old version of FMOD " << version << ". This program requires " << FMOD_VERSION << std::endl;
		return; 
	}
}

/*
* checks for drivers and soundcards and uses the correct modes dependant on results
* Code used directly from FMOD documentation for getting started for windows.
*/
void AudioManager::CheckDrivers()
{
	result = system->getNumDrivers(&numDrivers);
	FMODErrorCheck(result);
	if (numDrivers == 0)
	{
		result = system->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
	}
	else
	{
		result = system->getDriverCaps(0, &caps, 0, &speakerMode);
		FMODErrorCheck(result);

		//Set the user selected speaker mode.
		result = system->setSpeakerMode(speakerMode);
		FMODErrorCheck(result);
		if (caps & FMOD_CAPS_HARDWARE_EMULATED)
		{
			/*
			The user has the 'Acceleration' slider set to off! This is really bad
			for latency! You might want to warn the user about this.
			*/
			result = system->setDSPBufferSize(1024, 10);
			FMODErrorCheck(result);
		}
		result = system->getDriverInfo(0, name, 256, 0);
		FMODErrorCheck(result);
		if (strstr(name, "SigmaTel"))
		{
			/*
			Sigmatel sound devices crackle for some reason if the format is PCM 16bit.
			PCM floating point output seems to solve it.
			*/
			result = system->setSoftwareFormat(48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0,0,
				FMOD_DSP_RESAMPLER_LINEAR);
			FMODErrorCheck(result);
		}

	}
	result = system->init(100, FMOD_INIT_NORMAL, 0);
	if (result == FMOD_ERR_OUTPUT_CREATEBUFFER)
	{
		/*
		Ok, the speaker mode selected isn't supported by this soundcard. Switch it
		back to stereo...
		*/
		result = system->setSpeakerMode(FMOD_SPEAKERMODE_STEREO);
		FMODErrorCheck(result);
		/*
		... and re-init.
		*/
		result = system->init(100, FMOD_INIT_NORMAL, 0);
	}
	FMODErrorCheck(result);

}

/*
* A function to call all of the above init functions at once.
*/
void AudioManager::FMODInit()
{
	CreateSystem();
	FMODCheckVersion();
	CheckDrivers();
}
#pragma endregion

/*
* Used to create a sound. Use this for small sound files usually.
* Decompresses the whole sound into memory. Can use a lot of memory.
* @param - const char - sound to be loaded.
* @return - FMOD::Sound - returns sound.
*/
FMOD::Sound* AudioManager::CreateSound(const char *soundPath)
{
	result = system->createSound(soundPath, FMOD_DEFAULT, 0, &sound);
	FMODErrorCheck(result);
	return sound;
}

/*
* Use StreamSound to load and play large audio files. Uses much less memory
* but decodes at runtime, meaning it has to use cpu power at runtime.
* @param - const char - sound to be loaded.
* @return - FMOD::Sound - returns sound.
*/
FMOD::Sound *AudioManager::StreamSound(const char *soundPath)
{
	result = system->createStream(soundPath, FMOD_DEFAULT, 0, &sound);
	FMODErrorCheck(result);
	return sound;
}

/*
* Plays Looped sound.
* @param - FMOD::Sound - sound to be looped.
* @return - FMOD::Channel - returns channel.
*/
FMOD::Channel *AudioManager::PlayLoopedSound(FMOD::Sound *sound)
{
	result = system->playSound(FMOD_CHANNEL_FREE, sound, false, &channel);
	FMODErrorCheck(result);
	channel->setLoopCount(-1);
	channel->setMode(FMOD_LOOP_NORMAL);
	channel->setPosition(0, FMOD_TIMEUNIT_MS); // this flushes the buffer to ensure the loop mode takes effect
	return channel;
}

/*
* Plays single sound.
* @param - FMOD::Sound - sound to be played.
*/
void AudioManager::PlaySingleSound(FMOD::Sound *sound)
{
	result = system->playSound(FMOD_CHANNEL_FREE, sound, false, 0);
	FMODErrorCheck(result);
}

/*
* Pauses sound.
* @param - FMOD::Channel - channel to be paused.
*/
void AudioManager::PauseSound(FMOD::Channel *channel)
{
	channel->setPaused(true);
}

/*
* Unpauses sound.
* @param - FMOD::Channel - channel to be unpaused.
*/
void AudioManager::PlaySoundAfterPause(FMOD::Channel *channel)
{
	channel->setPaused(false);
}

/*
* FMOD update.
*/
void AudioManager::FMODUpdate()
{
	system->update();
}