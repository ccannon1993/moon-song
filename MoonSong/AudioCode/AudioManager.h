/*
*  MoonSong - Conundrum
*  AudioManager.h
*  Last Reviewed: 08/05/2014
*/
#pragma once

#include"fmod.hpp"
#include"fmod_errors.h"
#include<iostream>

class AudioManager
{
public:
	AudioManager(void);
	~AudioManager(void);
	void FMODInit();
	void FMODUpdate();
	FMOD::Sound* CreateSound(const char *soundPath);
	FMOD::Sound* StreamSound(const char *soundPath);
	void PlaySingleSound(FMOD::Sound *sound);
	FMOD::Channel *PlayLoopedSound(FMOD::Sound *sound);
	void PauseSound(FMOD::Channel *channel);
	void PlaySoundAfterPause(FMOD::Channel *channel);
private:
	void FMODErrorCheck(FMOD_RESULT result);
	void CreateSystem();
	void FMODCheckVersion();
	void CheckDrivers();

	FMOD::System *system;
	FMOD::Sound *sound;
	FMOD::Channel *channel;
	FMOD_RESULT result;
	FMOD_SPEAKERMODE speakerMode;
	FMOD_CAPS caps;

	unsigned int version;
	int numDrivers;

	char name[256];
};

