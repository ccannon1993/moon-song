/* 
* MoonSong - Conundrum
* fizzypop.cpp
* Last Reviewed: 08/05/2014
*
* FizzyPop simple physics handler
* 
* fizzypop.h
* fizzypop.cpp
* 
* FizzyPop is an action-oriented physics tracker that keeps a list what happened 
* each physics tick.
* Objects should be given the appropriate behaviour and registered with 
* FizzyPop. All objects with this behaviour will act according to the 
* Newtonian Laws of Motion when a physics update is called.
* FizzyPop only records any changes that occur to the current physics model.
* This means far less redundant data is stored than if the entire model were
* recorded each tick.
* 
* FizzyPop supports (or will support in future revisions) several extensions:
*		- BUBBLE (Back Up that Big BackLog Extension)
*			BUBBLE allows Fizzy's action list to be saved at set intervals 
*			(eg. every 5 seconds). Saving often will cause less playtime to be
*			lost in the event of a crash or power outage.
*		- Fizzy Keys
*			Fizzy Keys allows keymaps to be registered with objects, and lists
*			to be passed in each tick containing the keys pressed in that tick.
* 
* Copyright (c) 2014 Sean McKay (Teguki)
* 
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use,
* copy, modify, merge, publish, distribute, sublicense, and/or
* sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
* ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
* CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
*/

#ifndef TEGUKI_SODA
#define TEGUKI_SODA

#include <Windows.h>
#include <vector>
//#include <map>
#include "glm\glm.hpp"
#include "../Quaternion/quaternion.h"

#define FP_INV30 float( 100 / 3 )
#define FP_FIXED 0
#define FP_MOBILE 1

namespace fizz{

	class fizz
	{
		friend class fizzypop;
		friend class fizzykeys;
		friend class fizzyAIlight;
	protected:
		struct movement{
			glm::vec3 vel;
			glm::vec3 acc;
		};
		struct rotation{
			glm::quat angvel;
			glm::quat angacc;
		};

		glm::vec3 pos;
		movement* mov;
		glm::quat ori;
		rotation* rot;
		int tracked;

		fizz(int, int);
		~fizz(void);

		void interpolate( fizz*, float );
	public:
		void update();
		fizz(void);
		fizz* copy();
		int setPos( glm::vec3 ipos );
		int setVel( glm::vec3 ivel );
		int setAcc( glm::vec3 iacc );
		int setOri( glm::quat iori );
		int setAngVel( glm::quat iangvel );
		int setAngAcc( glm::quat iangacc );
		glm::vec3 getPos();
		glm::vec3 getVel();
		glm::vec3 getAcc();
		glm::quat getOri();
		glm::quat getAngVel();
		glm::quat getAngAcc();
		fizz* apply(fizz* alteration);
	};

	/*****************************************************************************/

	class fExt;

	class fizzypop
	{
		friend class fizzykeys;
		friend class fizzyAIlight;
	private:
		struct order{
			//action lists aren't fully implemented yet
			int ID;
			//some format for storing the changes. Maybe just an instance of fizz?
		};
		struct line{
			int time;
			std::vector< order > me;
			order getOrder(int);
			line(void);
			line(int);
			~line(void);
		};
		struct page{
			std::vector< line > me;
			void newLine(void);
			void newLine(int);
			line getLine(int);
			int getLength(void);
			page(void);
			~page(void);
		};

		std::vector< fExt* > extension;

		page actionList;

		int recordI;
		float recordTime;
		int startTime;

		std::vector< fizz* > record;
		std::vector< fizz* > bottle;

		void newline();
	public:
		// Update physics and add a new line to 
		void update();
		// make new objects
		fizz* buildFizz();
		fizz* buildFizz(int type);
		fizz* buildFizz(int movement_type, int rotation_type);
		// Register existing objects
		int attach( fizz* );
		void attach( fExt* );
		void detach( int obj );

		fizzypop(void);
		~fizzypop(void);
	};

}

#endif