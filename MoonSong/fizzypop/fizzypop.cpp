/*
*  MoonSong - Conundrum
*  fizzypop.h
*  Last Reviewed: 08/05/2014
*/

#include "../fizzypop/fizzypop.h"
#include "../fext/fext.h"
#include <iostream>

namespace fizz{

	/* 
	 * 
	 * Add new line if necessary, call all extensions, then interpolate for current time
	 * 
	 */
	void fizzypop::update()
	{
		for(unsigned int i = 0; i < extension.size(); i++){
			extension[i]->update( this );
		}
		float frameTime = ( GetTickCount() - startTime ) - recordTime;

		if( frameTime >= FP_INV30 )
		{
			//if time is too far ahead, add a new frame
			frameTime -= FP_INV30;
			recordTime = FP_INV30 * recordI;
			newline();
			//std::cout << "New frame!";
			if( frameTime >= FP_INV30 )
			{
				recordTime = 0;
				startTime = GetTickCount();
				frameTime = ( GetTickCount() - startTime ) - recordTime;
				//if time is still too far ahead, assume simulation was paused and store the new start time
				//recordTime = GetTickCount();
				//actionList.getLine( actionList.getLength() - 1 ).time = recordTime;
			}
		}
		//std::cout << frameTime;

		float framePercent = frameTime/FP_INV30;
		for(int i = 0, size = bottle.size(); i < size; i++)
		{
			if( bottle[i] ) bottle[i]->interpolate( record[i], framePercent );
		}
	}

	fizz* fizzypop::buildFizz(){
		return buildFizz(FP_MOBILE);
	}

	fizz* fizzypop::buildFizz(int type)
	{
		return buildFizz(type, type);
	}

	fizz* fizzypop::buildFizz(int movement_type, int rotation_type)
	{
		return new fizz(movement_type, rotation_type);
	}

	void fizzypop::newline()
	{
		for(int i = 0, size = record.size(); i < size; i++)
			record[i]->update();
		actionList.newLine();
	}

	int fizzypop::attach( fizz* in )
	{
		for(int i = 0; i < bottle.size(); i++)
			if( !bottle[i] ){
				bottle[i] = in;
				in->tracked++;
				fizz* rec = in->copy();
				record[i] = rec;
				return i;
			}
		
		bottle.push_back( in );
		in->tracked++;
		fizz* rec = in->copy();
		record.push_back( rec );
		return (record.size() - 1);
	}

	void fizzypop::attach( fExt* ext )
	{
		extension.push_back( ext );
	}

	void fizzypop::detach( int obj )
	{
		bottle[obj]->tracked--;
		bottle[obj] = nullptr; 
		record[obj] = nullptr; 
	}

	fizzypop::fizzypop(void)
	{
		actionList.newLine();
		recordTime = 0;
		recordI = 0;
		startTime = GetTickCount();
	}

	fizzypop::~fizzypop(void)
	{

	}

	/*****************************************************************************/

	void fizzypop::page::newLine(int unpauseTime)
	{
		if( unpauseTime ) // is not zero
			me.push_back( line( unpauseTime ) );
		else
			me.push_back( line() );
	}

	void fizzypop::page::newLine()
	{
		newLine( 0 );
	}

	fizzypop::page::page()
	{

	}

	fizzypop::page::~page()
	{

	}

	fizzypop::line::line()
	{

	}

	fizzypop::line::line(int unpauseTime)
	{

	}

	fizzypop::line::~line()
	{

	}

}