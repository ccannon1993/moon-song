/*
*  MoonSong - Conundrum
*  Quaternion.cpp
*  Last Reviewed: 08/05/2014
*/

#include"quaternion.h"

namespace{
	const double DEG = 0.0174532925;
}

namespace quat{

	/* 
	 * 
	�*�Use this function to generate a default orientation quaternion.
	�* 
	�*�@return�Function returns a default orientation quaternion.
	�* 
	�*/
	glm::quat eulerToQuat()
	{
		return glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));
	}

	/* 
	 * 
	�*�Use this function to initialise a quaternion. Angles are applied in the 
	�* order: Yaw, Pitch, Roll.
	�* 
	�*�@param��Function takes three Euler angles as floats: euler_pitch, euler_yaw, euler_roll.
	�*�@return�Function returns the corrosponding quaternion.
	�* 
	�*/
	glm::quat eulerToQuat(float euler_pitch, float euler_yaw, float euler_roll)
	{
		return glm::normalize(glm::quat(glm::vec3(0.0f, 0.0f, euler_roll * DEG)) * glm::quat(glm::vec3(euler_pitch * DEG, 0.0f, 0.0f)) * glm::quat(glm::vec3(0.0f, euler_yaw * DEG, 0.0f)));
	}

	glm::quat pointsToQuat(glm::vec3 ivec1, glm::vec3 ivec2)
	{
		ivec1 = glm::normalize(ivec1);
		ivec2 = glm::normalize(ivec2);
		glm::vec3 cross = glm::cross(ivec1, ivec2);
		glm::quat out = glm::quat(1.0f + glm::dot(ivec1, ivec2), cross.x, cross.y, cross.z);
		return glm::normalize(out);
	}

	/* 
	 * 
	�*�Use this function to convert a quaternion to a rotation matrix to rotate 
	 * objects in OpenGL. The quaternion is automatically normalised beforehand.
	�* (Unnormalised quaternions can distort objects in the viewport.)
	�* 
	�*�@param�glm::quat iquaternion t�akes any quaternion.
	�*�@return�Function returns the corrosponding rotation matrix.
	�* 
	�*/
	glm::mat4 quatToMat4(glm::quat iquaternion)
	{
		return glm::toMat4(glm::normalize(iquaternion));
	}

	/* 
	 * 
	�*�Use this function to rotate quaternions by other quaternions.
	�* 
	�*�@param��glm::quat iquaternion takes the quaternion to be rotated.
	�*�@param��glm::quat rotation takes the rotation to be applied.
	�*�@return�Function returns the state of iquaternion after rotation.
	�* 
	�*/
	glm::quat rotate(glm::quat iquaternion, glm::quat rotation)
	{
		return glm::normalize(rotation * iquaternion);
	}

	/* 
	 * 
	�*�Use this function to rotate quaternions by Euler angles.
	�* 
	�*�@param��glm::quat iquaternion takes the quaternion to be rotated.
	�*�@param��Function takes three Euler angles as floats: euler_pitch, euler_yaw, euler_roll.
	�*�@return�Function returns the state of iquaternion after rotation.
	�* 
	�*/
	glm::quat rotate(glm::quat iquaternion, float euler_pitch, float euler_yaw, float euler_roll)
	{
		return rotate(iquaternion, eulerToQuat(euler_pitch, euler_yaw, euler_roll));
	}

	/* 
	 * 
	�*�Performs a rotation between two points on a 4D hypersphere.
	�* Use this function to smoothly transition between orientations.
	�* 
	�*�@param��glm::quat origin takes the starting orientation as a quaternion.
	 *�@param��glm::quat target takes the final orientation as a quaternion.
	�*�@param��float scale indicates how much of the rotation will be done.
	 *		0.0 = no rotation will be done; origin returned.
	 *		1.0 = complete rotation assumed; target returned.
	 *		2.0 = rotation equivalent to two complete rotations between origin and target returned.
	�*�@return�Function returns a quaternion representing the rotation between 
	 *		origin and target, multiplied by param scale.
	�* 
	 */
	glm::quat interpolate(glm::quat origin, glm::quat target, float scale)
	{
		if(scale==0.0f)
			return origin;
		else if(scale==1.0f)
			return target;
		else
			
			return glm::normalize(glm::slerp(origin, target, scale));
	}

	/* 
	 * 
	�*�Use this function to increase or decrease the magnitude of a rotation.
	�* 
	�*�@param��glm::quat iquaternion takes the quaternion to be scaled.
	�*�@param float scale takes a scaling factor.
	�*�@return�Function returns the state of iquaternion after scaling.
	�* 
	�*/
	glm::quat scale(glm::quat iquaternion, float scale)
	{
		return interpolate(glm::quat(), iquaternion, scale);
	}

	/* 
	 * 
	�*�Use this function to increase or decrease the magnitude of a rotation.
	�* 
	�*�@param��glm::quat iquaternion takes the quaternion to be scaled.
	�*�@param float angle takes the desired magnitude.
	�*�@return�Function returns the state of iquaternion after scaling.
	�* 
	�*/
	glm::quat scaleToAngle(glm::quat iquaternion, float angle)
	{
		float magnitude = mag(iquaternion);
		if(magnitude){
			return interpolate(glm::quat(), iquaternion, angle/mag(iquaternion));
		}else{
			return eulerToQuat( 0, 0, 0 );
		}
	}

	/* 
	 * 
	�*�Use this function to measure the magnitude of a rotation.
	�* 
	�*�@param��glm::quat iquaternion takes the quaternion to be measured.
	�*�@return�Function returns the arc angle of iquaternion in degrees.
	�* 
	�*/
	float mag(glm::quat iquaternion)
	{
		return glm::angle(iquaternion);
	}
	
	/* 
	 * 
	�*�Use this function to measure the angle between two orientations.
	�* 
	�*�@param��glm::quat iquat1 takes the first orientation.
	�*�@param��glm::quat iquat2 takes the second orientation.
	�*�@return�Function returns the arc angle between iquat1 and iquat2 in degrees.
	�* 
	�*/
	float angle(glm::quat iquat1, glm::quat iquat2)
	{
		return glm::angle( rotate(iquat1, scale(iquat2, -1)) );
	}

}