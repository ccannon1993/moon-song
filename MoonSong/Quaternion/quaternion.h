/* 
 *  MoonSong - Conundrum
 *  Quaternion.h
 *  Last Reviewed: 08/05/2014
 * 
 * Quaternions Cheat Sheet 1.2
 * 
 * quaternion.h quaternion.cpp
 * 
 * Handy functions for rotations by quaternions.
 * Use "quat::" to access functions, just as you would type "glm::" or "std::".
 * 
 * Due to Euler angles being pretty rubbish, it is advised that you initialise 
 * and rotate quaternions using only one Euler axis at a time to acheive a 
 * desired orientation, if you don't know the order in which rotations will be 
 * applied. Here, rotations are applied in the order: Yaw, Pitch, Roll.
 * 
 * eulerToQuat() - produces a quaternion based on 3 Euler angles. Use function 
 *		without parameters to generate quaternion with no pitch, roll or yaw 
 *		(default orientation).
 * quatToMat4() - converts a quaternion to a rotation matrix.
 * rotate() - applies a rotation to a supplied quaternion. Function can rotate by 
 *		Euler angles or by another quaternion.
 * interpolate() - shifts a given quaternion smoothly from one orientation to another 
 *		(eg. by a function of time).
 * scale() - increases or decreases the size of a rotation. Useful function for applying 
 *		friction (where "a" is the portion of rotation that is kept).
 * mag() - returns the angle of the arc represented by a quaternion.
 * angle() - returns the angle of the arc between two quaternions' endpoints.
 * 
 * Created: February 2014, by Sean McKay (Teguki)
 * Last modified: February 2014, by Sean McKay (Teguki)
 * 
 * Copyright (c) 2014 Sean McKay (Teguki)
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef TEGUKI_QUAT
#define TEGUKI_QUAT

//#include"glm-0.9.4.4\glm\gtc\quaternion.hpp"
//#include"glm-0.9.4.4\glm\gtx\quaternion.hpp"
#include "glm\gtc\quaternion.hpp"
#include "glm\gtx\quaternion.hpp"


namespace quat{
	
	glm::quat eulerToQuat();

	glm::quat eulerToQuat(float euler_pitch, float euler_yaw, float euler_roll);

	glm::quat pointsToQuat(glm::vec3, glm::vec3);

	glm::mat4 quatToMat4(glm::quat iquaternion);

	glm::quat rotate(glm::quat iquaternion, glm::quat rotation);

	glm::quat rotate(glm::quat iquaternion, float euler_pitch, float euler_yaw, float euler_roll);

	glm::quat interpolate(glm::quat origin, glm::quat target, float scale);

	glm::quat scale(glm::quat iquaternion, float scale);

	glm::quat scaleToAngle(glm::quat iquaternion, float angle);

	float mag(glm::quat iquaternion);

	float angle(glm::quat, glm::quat);

}

#endif