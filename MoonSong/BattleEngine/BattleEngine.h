/*
*  MoonSong - Conundrum
*  BattleEngine.h
*  Last Reviewed: 08/05/2014
*/

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include "../BoundingBox/BoundingBox.h"
#include "../SphericalBounding/SphericalBounding.h"
#include "../RenderObject/renderObject.h"

#ifndef BATTLEENGINE_H  
#define BATTLEENGINE_H

class BattleEngine
{
private: 	
	bool inShop1;
	std::vector<BoundingBox *> boxes;	
	std::vector<SphericalBounding *> spheres;		
	bool DoSBBBIntersect(BoundingBox * bb, SphericalBounding * sb);	
	glm::vec3 DoLinePlaneIntersect(glm::vec3 startPos, glm::vec3 lineDirection, glm::vec3 endPos,  BoundingBox * bb);
	glm::vec3 DoLineSphereIntersect(glm::vec3 startPos, glm::vec3 lineDirection, glm::vec3 endPos, SphericalBounding * sb);
	void CalculateDamageHitScan(RenderObject &obj, RenderObject objFiring);
	void CalculateDamagePlanets(RenderObject &obj);	
	void CalculateDamageShips(RenderObject &ship1, RenderObject &ship2);
public:	
	BattleEngine();
	~BattleEngine();
	bool GetShop1() {return inShop1;}
	void SetShop1(bool in) {inShop1 = in;}
	void Init(void);
	void CreateBounding(std::string inID, std::string inName);
	void DeleteBounding(std::string id, std::string n);
	void DeleteAllBounding();
	BoundingBox* GetBox(std::string n);		
	SphericalBounding* GetSphere(std::string n);	
	void CheckCollision(std::vector<RenderObject *> &objectList);	
	glm::vec3 HitScan(glm::vec3 startPos, glm::vec3 direction, glm::vec3 endPos, std::vector<RenderObject *> objectList, RenderObject objFiring);	
};

#endif