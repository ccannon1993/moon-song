/*
*  MoonSong - Conundrum
*  BattleEngine.cpp
*  Last Reviewed: 08/05/2014
*  Bounding boxes and spherical boxes held in vector separating them from each other.
*  Methods are held for creating boundings and deleting single bounding or all.
*  Methods held for collisions: box to sphere, line to plane, line to sphere.
*  Function held for object collision and hitscan collision.
*  Methods held for calculate damage which are called when certain collisions occur.
*/

#include "BattleEngine.h"
#include <iostream>

/*
*  Constructor - Currently does nothing.
*/
BattleEngine::BattleEngine()
{
	inShop1 = false;
}

/*
*  Deconstructor
*  Deletes bounding types from vectors as created on heap.
*/
BattleEngine::~BattleEngine()
{
	DeleteAllBounding();
} 

/*
*  Nothing in function at moment.
*  May be required later date.
*/
void BattleEngine::Init(void)
{	
	
}

/*
*  Creates bounding and adds to vector type that it is appropriate to.
*  @param - std::string - ID of object.
*  @param - std::string - name of object.
*  Will give error output if type matches no options - test purposes.
*/
void BattleEngine::CreateBounding(std::string inID, std::string inName)
{
	if(inID == "PLANET")
	{
		SphericalBounding * sb = new SphericalBounding(inID, inName);
		spheres.push_back(sb);
	}
	else if(inID == "SPACESHIP")
	{
		BoundingBox * bb = new BoundingBox(inID, inName);
		boxes.push_back(bb);		
	}
	else if(inID == "ENEMY")
	{
		BoundingBox * bb = new BoundingBox(inID, inName);
		boxes.push_back(bb);
	}
	else if(inID == "ROCKET")
	{
		BoundingBox * bb = new BoundingBox(inID, inName);
		boxes.push_back(bb);
	}	
	else
		std::cout << "Error - cannot create bounding as type does not match options." << std::endl;
}

/*
*  Deletes bounding box from the appropriate vector.
*  Finds vector with type so does not need to search all vectors, finds 
*  bounding with name.
*  Will skip vector if empty.
*  @param - std::string - name of bounding to delete.
*  Will give error output if type matches no options - test purposes.
*/
void BattleEngine::DeleteBounding(std::string id, std::string n)
{
	bool found = false;
	int index = 0;

	if(boxes.size() > 0 && (id == "ASTEROID" || id == "SPACESHIP" || id == "ENEMY" || id == "ROCKET"))
	{
		while(index < boxes.size() && found != true)
		{
			if(boxes[index]->GetName() == n)
				found = true;
			else
				index++;
		}		
		delete boxes[index];
		boxes[index] = NULL;
		boxes.erase(boxes.begin() + index);	
	}	
	
	if(spheres.size() > 0 && id == "PLANET")
	{
		index = 0; //reset as searching new vector
		while(index < spheres.size() && found != true)
		{
			if(spheres[index]->GetName() == n)
				found = true;
			else
				index++;
		}		
		delete spheres[index];
		spheres[index] - NULL;
		spheres.erase(spheres.begin() + index);	
	}
			
	if(found == false)
		std::cout << "Error - cannot delete bounding box as name doesn't exist." << std::endl;
	
}

/*
*  Deletes all bounding from all vectors.
*/
void BattleEngine::DeleteAllBounding()
{	
	if(boxes.size() > 0)
	{		
		for(int i = 0; i < boxes.size(); ++i)
		{
			delete boxes[i];
			boxes[i] = NULL;
		}			
		boxes.clear();			
	}

	if(spheres.size() > 0)
	{
		for(int i = 0; i < spheres.size(); ++i)
		{
			delete spheres[i];
			spheres[i] = NULL;
		}			
		spheres.clear();		
	}		
}

/*
*  Gets bounding box in boxes vector.
*  @param - std::string - name of bounding box.
*  @return - BoundingBox* - returns bounding box found. 
*/
BoundingBox* BattleEngine::GetBox(std::string n)
{	
	bool found = false;
	int i = 0;
	while(i < boxes.size() && found != true)
	{
		if(boxes[i]->GetName() == n)
			found = true;
		else 
			i++;
	}	
	if(found == false)
		std::cout << "Name doesn't exist" << std::endl;

	return boxes[i];
}

/*
*  Gets sphere in spheres vector.
*  @param - std::string - name of sphere.
*  @return - SphericalBounding* - returns sphere found. 
*/
SphericalBounding* BattleEngine::GetSphere(std::string n)
{
	bool found = false;
	int i = 0;
	while(i < spheres.size() && found != true)
	{
		if(spheres[i]->GetName() == n)
			found = true;
		else 
			i++;
	}	
	if(found == false)
		std::cout << "Name doesn't exist" << std::endl;

	return spheres[i];
}

/*
*  Takes in a bounding box and sphere and will check if collision occurred. 
*  It does this by checking where the closest point of the box is in relation to the
*  sphere, once the point is found will then check if intersects using the distance
*  formula. Code was taken and changed from here:
*  http://www.gamedev.net/topic/335465-is-this-the-simplest-sphere-aabb-collision-test/
*  @param - BoundingBox - the given bounding box to check collision against.
*  @param - SphericalBounding - the given sphere to check collision against.
*  @return - boolean - result of intersect check, true if occurs, false otherwise.
*/
bool BattleEngine::DoSBBBIntersect(BoundingBox * bb, SphericalBounding * sb)
{		
	//Get the centre of the sphere relative to the centre of the box
	glm::vec3 sphereCentreRelBox = sb->GetCentre() - bb->GetCentre();
	
	//Point on surface of box that is closest to the centre of the sphere
	glm::vec3 boxPoint;
	
	//Check sphere centre against box along the X axis alone. 
	//If the sphere is off past the left edge of the box, then the left edge
	//is closest to the sphere. Similar if it's past the right edge. 
	//If it's between the left and right edges, then the sphere's own X is
	//closest, because that makes the X distance 0.
	
	if (sphereCentreRelBox.x < -bb->GetLength()/2)
		boxPoint.x = -bb->GetLength()/2;
	else if (sphereCentreRelBox.x > bb->GetLength()/2)
		boxPoint.x = bb->GetLength()/2;
	else
		boxPoint.x = sphereCentreRelBox.x;
	
	//Same for Y axis
	if (sphereCentreRelBox.y < -bb->GetHeight()/2.0)
		boxPoint.y = -bb->GetHeight()/2.0;
	else if (sphereCentreRelBox.y > bb->GetHeight()/2.0)
		boxPoint.y = bb->GetHeight()/2.0;
	else
		boxPoint.y = sphereCentreRelBox.y;
	
	//Same for Z axis
	if (sphereCentreRelBox.z < -bb->GetDepth()/2.0)
		boxPoint.z = -bb->GetDepth()/2.0;
	else if (sphereCentreRelBox.z > bb->GetDepth()/2.0)
		boxPoint.z = bb->GetDepth()/2.0;
	else
		boxPoint.z = sphereCentreRelBox.z;
	
	//Now we have the closest point on the box, so get the distance from 
	//that to the sphere center, and see if it's less than the radius	
	glm::vec3 dist = sphereCentreRelBox - boxPoint;		
	if ((dist.x*dist.x + dist.y*dist.y + dist.z*dist.z) < (sb->GetRadiusSquared()))
		return true;
	else
		return false;
}

/*
*  Checks all collisions. Three Checks in total.
*  Boxes against other boxes.
*  Boxes against spheres.
*  Spheres against other spheres.
*  If collision occurs it will get the correct object(s) and call the appropriate
*  damage function.
*  @param - std::vector - takes in list of objects held.
*/
void BattleEngine::CheckCollision(std::vector<RenderObject *> &objectList)
{	
	//Boxes against other boxes
	if(boxes.size() > 0)
	{
		for(int i = 0; i < boxes.size() - 1; i++)		
			for(int j = i + 1; j < boxes.size(); j++)
			{
				if(boxes[i]->DoBBIntersect(*boxes[j]) == true)
				{
 					int k = 0;
					int l = 0;
					while(boxes[i]->GetName() != objectList[k]->GetName())
						k++;					
					while(boxes[j]->GetName() != objectList[l]->GetName())
						l++;
					if((objectList[k]->GetID() == "SPACESHIP" || objectList[k]->GetID() == "ENEMY") && (objectList[l]->GetID() == "ENEMY" || objectList[l]->GetID() == "SPACESHIP"))
						CalculateDamageShips(*objectList[k], *objectList[l]);					
				}
			}	
	}

	inShop1 = false;
	//Boxes against spheres
	if(boxes.size() > 0 && spheres.size() > 0)
	{
		for(int i = 0; i < boxes.size(); i++)		
			for(int j = 0; j < spheres.size(); j++)
			{
				if(DoSBBBIntersect(boxes[i], spheres[j]) == true)
				{
					int k = 0;					
					while(boxes[i]->GetName() != objectList[k]->GetName())
						k++;		
					if(objectList[k]->GetName() == "Player" && spheres[j]->GetName() == "Shop1")
					{
    						objectList[k]->SetVelocity(glm::vec3(objectList[k]->GetVelocity().x - objectList[k]->GetVelocity().x, objectList[k]->GetVelocity().y - objectList[k]->GetVelocity().y, objectList[k]->GetVelocity().z - objectList[k]->GetVelocity().z));
						inShop1 = true;
					}
					else
						CalculateDamagePlanets(*objectList[k]);					
				}								
			}			
	}
		

	//Spheres against spheres
	//Should not happen at current design as spheres are only planets
	if(spheres.size() > 0)	
		for(int i = 0; i < spheres.size() - 1; i++)		
			for(int j = i + 1; j < spheres.size(); j++)
				if(spheres[i]->DoSBIntersect(*spheres[j]) == true)				
					std::cout << "Planets Colliding!! What the hell!!" << std::endl;		
}

/*
*  Checks if line intersects boxes.
*  Checks if line intersects spheres.
*  Will check if line intersects more than one object and will only deal with the closest to the position.
*  @param - glm::vec3 - position of the object firing.
*  @param - glm::vec3 - direction of the fire, objects look at vector.
*  @param - std::vector - takes in list of objects held.
*  @param - renderObject - takes in object that is firing as required if fire hits something.
*  @return - glm::vec3 - returns the end point of the laser or the point it intersects an object.
*/
glm::vec3 BattleEngine::HitScan(glm::vec3 startPos, glm::vec3 direction, glm::vec3 endPos, std::vector<RenderObject *> objectList, RenderObject objFiring)
{
	//Used to hold bounding nearest to the startPos.
	BoundingBox * bb = NULL;
	SphericalBounding * sb = NULL;	

	//Used to hold the position hit on an object.
	glm::vec3 hitPos;
	glm::vec3 actualHitBB;
	glm::vec3 actualHitSB;

	//Used to know which type of bounding need to check when nearest hit object found.
	bool BBHit = false;
	bool SBHit = false;
		
	for(int i = 0; i < boxes.size(); i++)
	{
		if(objFiring.GetName() == boxes[i]->GetName())
			;//ignore as checking object that is firing
		else
		{
			//Check if line intersects, if true set bb to box that is intersected.
			if(bb == NULL)
			{
				hitPos = DoLinePlaneIntersect(startPos, direction, endPos, boxes[i]);
				if(hitPos != endPos)
				{
					bb = boxes[i];
					actualHitBB = hitPos;
				}
			}
			//Check if line intersects, if true check if current bb held is closer than second bb intersected.
			if(bb != NULL)
			{
				hitPos = DoLinePlaneIntersect(startPos, direction, endPos, boxes[i]);
				if(hitPos != endPos)
				{
					glm::vec3 distCurrentBB = boxes[i]->GetCentre() - startPos;
					glm::vec3 distBB = bb->GetCentre() - startPos;
					//Set bb to new box as this one is closer than previous.
					if((distCurrentBB.x*distCurrentBB.x + distCurrentBB.y*distCurrentBB.y + distCurrentBB.z*distCurrentBB.z) <
					(distBB.x*distBB.x + distBB.y*distBB.y + distBB.z*distBB.z))
					{
						bb = boxes[i];
						actualHitBB = hitPos;
					}
				}
			}
		}		
	}	

	for(int i = 0; i < spheres.size(); i++)
	{
		//Check if line intersects, if true set sb to sphere that is intersected.
		if(sb == NULL)
		{
			hitPos = DoLineSphereIntersect(startPos, direction, endPos, spheres[i]);
			if(hitPos != endPos)
			{
				sb = spheres[i];
				actualHitSB = hitPos;
			}			
		}
		//Check if line intersects, if true check if current sb held is closer than second sb intersected.
		if(sb != NULL) 
		{
			hitPos = DoLineSphereIntersect(startPos, direction, endPos, spheres[i]);
			if(hitPos != endPos)
			{
				glm::vec3 distCurrentSB = spheres[i]->GetCentre() - startPos;                                                            
				glm::vec3 distSB = sb->GetCentre() - startPos;
				//Set sb to new box as this one is closer than previous.
				if((distCurrentSB.x*distCurrentSB.x + distCurrentSB.y*distCurrentSB.y + distCurrentSB.z*distCurrentSB.z) <
				(distSB.x*distSB.x + distSB.y*distSB.y + distSB.z*distSB.z))
				{
					sb = spheres[i];
					actualHitSB = hitPos;
				}
			}
		}		
	}
	
	//All objects now checked, require to check if an sb & bb are held and which is the closest if so.
	if(sb != NULL && bb != NULL)
	{
		glm::vec3 distBB = bb->GetCentre() - startPos;
		glm::vec3 distSB = sb->GetCentre() - startPos;
		if(distBB.x*distBB.x + distBB.y*distBB.y + distBB.z*distBB.z < distSB.x*distSB.x + distSB.y*distSB.y + distSB.z*distSB.z)
			BBHit = true;
		
		else		
			SBHit = true;	
	}
	else
	{
		if(sb != NULL)
			SBHit = true;
		if(bb != NULL)
			BBHit = true;
		//else nothing to check.
	}
	
	if(BBHit == true)
	{
		int k = 0;
		while(bb->GetName() != objectList[k]->GetName())
			k++;
		CalculateDamageHitScan(*objectList[k], objFiring);		
		return actualHitBB;
	}
	else if(SBHit == true) //No calculate damage required currently as only planets held as spheres. Planets cannot be damaged.
	{
		int k = 0;
		while(sb->GetName() != objectList[k]->GetName())
			k++;		
		return actualHitSB;
	}
	else
		return endPos;
	
}

/*
*  Checks if line intersects a plane of a bounding box.
*  Checks where the line intersects the plane, will then check if object is within distance of line.
*  Checks x, y & z planes.
*  @param - glm::vec3 - the initial point on the line.
*  @param - glm::vec3 - the direction of the line.
*  @param - BoundingBox - the box that has to be checked.
*  @return - glm::vec3 - returns the end point of the line or the point it intersects a box.
*/
glm::vec3 BattleEngine::DoLinePlaneIntersect(glm::vec3 startPos, glm::vec3 lineDirection, glm::vec3 endPos, BoundingBox * bb)
{	
	//X Plane
	//Require to calculate normal for plane.	
	//Calculate normal using two vectors on the plane.
	glm::vec3 p = glm::vec3(0, bb->GetHeight(), 0); 
	glm::vec3 q = glm::vec3(0, 0, bb->GetDepth()); 
	glm::vec3 n = glm::cross(p, q);
	//D = -n.p, where p is point on the plane.
	float D = glm::dot(-n, bb->GetCentre());
	//t = -(n.initial point) + D/n.lineDirection.
	float t = -(glm::dot(n, startPos) + D)/glm::dot(n, lineDirection);
	//Point(t) = S(initial point) + V(line direction)
	glm::vec3 pointOfIntersection = startPos + (t * lineDirection);
	//Check if line intersects y & z coordinates of box.
	if(pointOfIntersection.y >= bb->GetMin().y && pointOfIntersection.y <= bb->GetMax().y
	&& pointOfIntersection.z >= bb->GetMin().z && pointOfIntersection.z <= bb->GetMax().z)
	{
		//Check on intersected plane if point is between line start and end positions.
		if((pointOfIntersection.x < startPos.x && pointOfIntersection.x > endPos.x) 
		|| (pointOfIntersection.x > startPos.x && pointOfIntersection.x < endPos.x))
			return pointOfIntersection;
	}

	//Y plane
	//Same checks using details from the Y plane.
	p = glm::vec3(bb->GetLength(), 0, 0); //This vector is (min.x, max.y, min.x) - min.
	q = glm::vec3(0, 0, bb->GetDepth()); //This vector is (min.x, min.y, max.z) - min.
	n = glm::cross(p, q);
	D = glm::dot(-n, bb->GetCentre());
	t = -(glm::dot(n, startPos) + D)/glm::dot(n, lineDirection);
	pointOfIntersection = startPos + (t * lineDirection);
	if(pointOfIntersection.x >= bb->GetMin().x && pointOfIntersection.x <= bb->GetMax().x
	&& pointOfIntersection.z >= bb->GetMin().z && pointOfIntersection.z <= bb->GetMax().z)
	{		
		if((pointOfIntersection.y < startPos.y && pointOfIntersection.y > endPos.y) 
		|| (pointOfIntersection.y > startPos.y && pointOfIntersection.y < endPos.y))
			return pointOfIntersection;
	}
				
	//Z plane
	//Same checks using details from the Z plane.
	p = glm::vec3(bb->GetLength(), 0, 0); 
	q = glm::vec3(0, bb->GetHeight(), 0);
	n = glm::cross(p, q);
	D = glm::dot(-n, bb->GetCentre());
	t = -(glm::dot(n, startPos) + D)/glm::dot(n, lineDirection);
	pointOfIntersection = startPos + (t * lineDirection);
	if(pointOfIntersection.x >= bb->GetMin().x && pointOfIntersection.x <= bb->GetMax().x
	&& pointOfIntersection.y >= bb->GetMin().y && pointOfIntersection.y <= bb->GetMax().y)			
	{		
		if((pointOfIntersection.z < startPos.z && pointOfIntersection.z > endPos.z) 
		|| (pointOfIntersection.z > startPos.z && pointOfIntersection.z < endPos.z))
			return pointOfIntersection;
	}

	//No intersections
	return endPos;
}

/*
*  Checks if line intersects a sphere.
*  Using an start and end point on the line will check if intersects the sphere.
*  @param - glm::vec3 - the initial point on the line.
*  @param - glm::vec3 - the direction of the line.
*  @param - SphericalBounding - the sphere that has to be checked.  
*  @return - glm::vec3 - returns the end point of the line or the point it intersects a sphere.
*/
glm::vec3 BattleEngine::DoLineSphereIntersect(glm::vec3 startPos, glm::vec3 lineDirection, glm::vec3 endPos, SphericalBounding * sb)
{	
	//Calculate quadratic function variables so can then use discriminant to find out if line intersects sphere.
	//a = (endPoint - linePos)squared // b = 2((endPoint - linePos).(linePos-centre) // c = (linePos-centre)squared - radius*radius.
	float a = (endPos.x - startPos.x)*(endPos.x - startPos.x) + (endPos.y - startPos.y)*(endPos.y - startPos.y) + 
		(endPos.z - startPos.z)*(endPos.z - startPos.z);
	float b = 2 * ((endPos.x - startPos.x)*(startPos.x - sb->GetCentre().x) + (endPos.y - startPos.y)*(startPos.y - sb->GetCentre().y) +
		(endPos.z - startPos.z)*(startPos.z - sb->GetCentre().z));
	float c = (startPos.x - sb->GetCentre().x)*(startPos.x - sb->GetCentre().x) + (startPos.y - sb->GetCentre().y)*(startPos.y - sb->GetCentre().y) +
		(startPos.z - sb->GetCentre().z)*(startPos.z - sb->GetCentre().z) - sb->GetRadiusSquared();
	
	//discriminant delta.
	float delta = (b*b) - (4.0f*a*c);
	
	if(delta < 0) //no collision
		return endPos;
	else if(delta == 0) //one point collision
	{
		//find point of intersection using parametric equations
		float d = -b/(2*a); 
		glm::vec3 pointOfIntersection;
		pointOfIntersection.x = startPos.x + d*(endPos.x-startPos.x);
		pointOfIntersection.y = startPos.y + d*(endPos.y-startPos.y);
		pointOfIntersection.z = startPos.z + d*(endPos.z-startPos.z);
		//Check if point of intersection is between start and end position of the line on any of the axes.
		if((pointOfIntersection.x > startPos.x && pointOfIntersection.x < endPos.x) || (pointOfIntersection.x < startPos.x && pointOfIntersection.x > endPos.x)
			||(pointOfIntersection.y > startPos.y && pointOfIntersection.y < endPos.y) || (pointOfIntersection.y < startPos.y && pointOfIntersection.y > endPos.y)
			|| (pointOfIntersection.z > startPos.z && pointOfIntersection.z < endPos.z) || (pointOfIntersection.z < startPos.z && pointOfIntersection.z > endPos.z))
			return pointOfIntersection;
		else //point of intersection is not between the line points
			return endPos;
	}
	else //delta > 0 (two intersection points)
	{
		//find points of intersection using parametric equations
		float d1 = (-b-sqrt(delta))/(2*a);
		float d2 = (-b+sqrt(delta))/(2*a);
		glm::vec3 pointOfIntersection1;
		pointOfIntersection1.x = startPos.x + d1*(endPos.x-startPos.x);
		pointOfIntersection1.y = startPos.y + d1*(endPos.y-startPos.y);
		pointOfIntersection1.z = startPos.z + d1*(endPos.z-startPos.z);
		glm::vec3 pointOfIntersection2;
		pointOfIntersection2.x = startPos.x + d2*(endPos.x-startPos.x);
		pointOfIntersection2.y = startPos.y + d2*(endPos.y-startPos.y);
		pointOfIntersection2.z = startPos.z + d2*(endPos.z-startPos.z);

		//used to calculate distances of intersection points
		glm::vec3 distPoint1 = pointOfIntersection1 - startPos;
		glm::vec3 distPoint2 = pointOfIntersection2 - startPos;

		//Check if points of intersection are between start and end position of the line on any of the axes.
		if((pointOfIntersection1.x > startPos.x && pointOfIntersection1.x < endPos.x) || (pointOfIntersection1.x < startPos.x && pointOfIntersection1.x > endPos.x)
		||(pointOfIntersection1.y > startPos.y && pointOfIntersection1.y < endPos.y) || (pointOfIntersection1.y < startPos.y && pointOfIntersection1.y > endPos.y)
		|| (pointOfIntersection1.z > startPos.z && pointOfIntersection1.z < endPos.z) || (pointOfIntersection1.z < startPos.z && pointOfIntersection1.z > endPos.z)
		|| (pointOfIntersection2.x > startPos.x && pointOfIntersection2.x < endPos.x) || (pointOfIntersection2.x < startPos.x && pointOfIntersection2.x > endPos.x)
		||(pointOfIntersection2.y > startPos.y && pointOfIntersection2.y < endPos.y) || (pointOfIntersection2.y < startPos.y && pointOfIntersection2.y > endPos.y)
		|| (pointOfIntersection2.z > startPos.z && pointOfIntersection2.z < endPos.z) || (pointOfIntersection2.z < startPos.z && pointOfIntersection2.z > endPos.z))
			if((distPoint1.x*distPoint1.x + distPoint1.y*distPoint1.y + distPoint1.z*distPoint1.z) < //Check which point is closest
			(distPoint2.x*distPoint2.x + distPoint2.y*distPoint2.y + distPoint2.z*distPoint2.z))
				return pointOfIntersection1;
			else			
				return pointOfIntersection2;
		else
			return endPos;	
	}
}

/*
*  Calculates damage for hitscan.
*  Will check first what type of object is to be damaged and then update.
*  @param - renderObject - the object that is to be damaged.  
*  @param - int - takes in the mass of object firing, will allow heavier ships to inflict higher damage.
*/
void BattleEngine::CalculateDamageHitScan(RenderObject &obj, RenderObject objFiring)
{
	float LASER_FIRE = objFiring.GetFireDamage();
	if(obj.GetID() == "ENEMY")
	{		
		//Reduced damage on asteroids compared to ships
		float enemyHealth = obj.GetHealth() - LASER_FIRE;
		obj.SetHealth(enemyHealth);
		obj.SetTexture(6); //Sets texture to white as hit
	}
	else if(obj.GetID() == "SPACESHIP")
	{		
		float shipHealth = obj.GetHealth() - LASER_FIRE;
		obj.SetHealth(shipHealth);
		obj.SetTexture(6); //Sets texture to white as hit
	}
	else
		std::cout << "Error - HitScan collision detection has received invalid type" << std::endl;
}

/*
*  Sets ship health to zero as collided with planet.
*  @param - renderObject - the object that is to be destroyed.  
*/
void BattleEngine::CalculateDamagePlanets(RenderObject &obj)
{	 
	 obj.SetHealth(0);	 
}

/*
*  Calculates damage for ship to ship collisions.
*  Function can also be used for asteroid to asteroid collision.
*  Damage based on mass and a constant 
*  New health set for both ships after damage calculated.
*  @param - renderObject - the object that is to be damaged.  
*  @param - renderObject - the object that is to be damaged.
*/
void BattleEngine::CalculateDamageShips(RenderObject &ship1, RenderObject &ship2)
{		
	float COLLISION_DAMAGE = 0.1;
	float shipHealth1 = ship1.GetHealth() - (COLLISION_DAMAGE * ship2.GetMass());
	float shipHealth2 = ship2.GetHealth() - (COLLISION_DAMAGE * ship1.GetMass());
	ship1.SetHealth(shipHealth1);
	ship2.SetHealth(shipHealth2);	
	glm::vec3 ship1Vel = ship1.GetVelocity(), ship2Vel = ship2.GetVelocity();	
	glm::vec3 nShip1, nShip2;
	if(ship1Vel.x != 0 || ship1Vel.y != 0 || ship1Vel.z != 0)
		nShip1 = glm::normalize(ship1Vel);	
	else
		nShip1 = glm::vec3(0.0, 0.0, 0.0);
	if(ship2Vel.x != 0 || ship2Vel.y != 0 || ship2Vel.z != 0)
		nShip2 = glm::normalize(ship2Vel);
	else
		nShip2 = glm::vec3(0.0, 0.0, 0.0);
	glm::vec3 movement1, movement2;
	if(ship1.GetVelocity().x < 0) movement1.x = -5;	else movement1.x = 5;
	if(ship1.GetVelocity().y < 0) movement1.y = -5;	else movement1.y = 5;
	if(ship1.GetVelocity().z < 0) movement1.z = -5; else movement1.z = 5;
	if(ship2.GetVelocity().x < 0) movement2.x = -5; else movement2.x = 5;
	if(ship2.GetVelocity().y < 0) movement2.y = -2; else movement2.y = 5;
	if(ship2.GetVelocity().z < 0) movement2.z = -5;	else movement2.z = 2;

	float vel1T = ship1.GetVelocity().x*ship1.GetVelocity().x + ship1.GetVelocity().y*ship1.GetVelocity().y + ship1.GetVelocity().z*ship1.GetVelocity().z;
	float vel2T = ship2.GetVelocity().x*ship2.GetVelocity().x + ship2.GetVelocity().y*ship2.GetVelocity().y + ship2.GetVelocity().z*ship2.GetVelocity().z;

	ship1.SetVelocity(glm::vec3(ship1.GetVelocity().x - ship1.GetVelocity().x, ship1.GetVelocity().y - ship1.GetVelocity().y, ship1.GetVelocity().z - ship1.GetVelocity().z));
	ship2.SetVelocity(glm::vec3(ship2.GetVelocity().x - ship2.GetVelocity().x, ship2.GetVelocity().y - ship2.GetVelocity().y, ship2.GetVelocity().z - ship2.GetVelocity().z));
	ship1.SetLaserDisabled(true);
	ship2.SetLaserDisabled(true);
	ship1.SetLaserStart(clock());
	ship1.SetLaserFinish(clock() + 1000);
	ship2.SetLaserStart(clock());
	ship2.SetLaserFinish(clock() + 1000);
	if(vel1T > vel2T)
	{
		ship2.SetVelocity(glm::vec3(ship1Vel.x/2, ship1Vel.y/2, ship1Vel.z/2));
		ship1.SetPosition(glm::vec3(ship1.GetPosition().x - nShip2.x * movement2.x, ship1.GetPosition().y + nShip2.y * movement2.y, ship1.GetPosition().z + nShip2.z * movement2.z));
		ship2.SetPosition(glm::vec3(ship2.GetPosition().x - nShip1.x * movement1.x, ship2.GetPosition().y + nShip1.y * movement1.y, ship2.GetPosition().z + nShip1.z * movement1.z));
	}	
	else
	{
		ship1.SetVelocity(glm::vec3(ship2Vel.x/2, ship2Vel.y/2, ship2Vel.z/2));
		ship1.SetPosition(glm::vec3(ship1.GetPosition().x - nShip2.x * movement2.x, ship1.GetPosition().y + nShip2.y * movement2.y, ship1.GetPosition().z + nShip2.z * movement2.z));
		ship2.SetPosition(glm::vec3(ship2.GetPosition().x - nShip1.x * movement1.x, ship2.GetPosition().y + nShip1.y * movement1.y, ship2.GetPosition().z + nShip1.z * movement1.z));
	}	
}