/*
*  MoonSong - Conundrum
*  Player.h
*  Last Reviewed: 08/05/2014
*/

#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Player
{
public:
	Player(void);
	Player(int money);
	~Player(void);
	int GetPlayerMoney(){return money;}
	int GetCopper(){return copper;}
	int GetGold(){return gold;}
	int GetPlatinum(){return platinum;}
	int GetItem(std::string item);	
	void AddItem(std::string inItem, int inAmount);
	void RemoveItem(std::string inItem, int inAmount);
	void UpdateMoney(int inMoney);
	void UpdateHealth(int inHealth);
	void LoadStats(const char* fileName);
	int ReturnPlayerHealth(){return health;}
	int ReturnPrimaryAmmo(){return primaryAmmo;}
	int ReturnSecondaryAmmo(){return secondaryAmmo;}
	//const char returnPrimaryAmmoString(){return primaryAmmo.c_str();}
	void ShootPrimary();
	int SaveGame(const char* fname);

	//weapons
	glm::vec3 GetFireColour() {return fireColour;}
	float GetFireSize() {return fireSize;}
	float GetFireDamage() {return fireDamage;}

private:
	int money;
	int copper;
	int gold;
	int platinum;
	int primaryAmmo;
	int secondaryAmmo;
	int health;

	//weapons
	glm::vec3 fireColour;
	float fireSize;
	float fireDamage;

};

