/*
*  MoonSong - Conundrum
*  Player.cpp
*  Last Reviewed: 08/05/2014
*/

#include "player.h"

/*
* Constructor
*/
Player::Player(void)
{
}

/*
* Parameter Constructor
* @param - int - money player will have.
*/
Player::Player(int inMoney)
{
	money = inMoney;
	health = 100;

	fireColour = glm::vec3(1.0,0.0,0.0);
	fireSize = 1.0;
	fireDamage = 0.5;
}

/*
* Deconstructor
*/
Player::~Player(void)
{
}

/*
* Updates players money
* @param - int - money to be set to player.
*/
void Player::UpdateMoney(int inMoney)
{
	money = inMoney;
}

/*
* Gets item amount and returns it.
* @param - std::string - name of item to return.
* @return - int - the amount of the item.
*/
int Player::GetItem(std::string item)
{
	if(item == "Gold"){return gold;}
	if(item == "Platinum"){return platinum;}
	if(item == "Copper"){return copper;}
}

/*
* Adds items.
* @param - std::string - the name of item to be added.
* @param - int - the amount of item to be added.
*/
void Player::AddItem(std::string inItem, int inAmount)
{
	if(inItem == "Gold"){gold += inAmount;}
	if(inItem == "Platinum"){platinum += inAmount;}
	if(inItem == "Copper"){copper += inAmount;}
	if(inItem == "Pammo"){primaryAmmo += 50;}
	if(inItem == "Sammo"){secondaryAmmo += 2;}
	if(inItem == "repair"){health = 100;}
	if(inItem == "Light Laser")
	{
		fireColour = glm::vec3(1.0,0.0,0.0);
		fireSize = 1.0;
		fireDamage = 1;
	}
	if(inItem == "Medium Laser")
	{
		fireColour = glm::vec3(0.0,1.0,0.0);
		fireSize = 5.0;
		fireDamage = 5;
	}
	if(inItem == "Heavy Laser")
	{
		fireColour = glm::vec3(0.0,0.0,1.0);
		fireSize = 8.0;
		fireDamage = 10;
	}
}

/*
* Removes item from the player.
* @param - std::string - name of item to be removed.
* @param - int - the amount of item to be removed.
*/
void Player::RemoveItem(std::string inItem, int inAmount)
{
	if(inItem == "Gold"){gold -= inAmount;}
	if(inItem == "Platinum"){platinum -= inAmount;}
	if(inItem == "Copper"){copper -= inAmount;}	
}

/*
* Reduces ammo if the player is shooting.
*/
void Player::ShootPrimary()
{
	primaryAmmo--;
	if(primaryAmmo <= 0)
		primaryAmmo = 0;
}

/*
* Updates the players health.
* @param - int - the health to be updated.
*/
void Player::UpdateHealth(int inHealth)
{
	health = inHealth;
}

/*
* Loads the player statistics.
* @param - const char - the name of the text file to be loaded in.
*/
void Player::LoadStats(const char* fileName)
{
	int i = 0;

	std::string line1; 
	std::ifstream myfile (fileName);
	if (myfile.is_open())
	{
		while ( getline (myfile,line1) )
		{
			if(i == 0){
				health = atoi(line1.c_str());//Primary Ammo
			}
			if(i == 1){
				primaryAmmo = atoi(line1.c_str());//Primary Ammo
			}
			if(i == 2){
				secondaryAmmo = atoi(line1.c_str());//Secondary Ammo
			}
			if(i == 3){
				copper = atoi(line1.c_str());//Copper
			}
			if(i == 4){
				gold = atoi(line1.c_str());//Gold
			}
			if(i == 5){
				platinum = atoi(line1.c_str());//Platinum
			}i++;
		}
	}
}

/*
* Saves the player statistics.
* @param - const char - the name of the file to be saved to.
* @return - int - returned to confirmed saved.
*/
int Player::SaveGame(const char* fname)
{
	std::ofstream saveFile;
	saveFile.open (fname);
	saveFile << health << std::endl;
	saveFile << primaryAmmo << std::endl;
	saveFile << secondaryAmmo << std::endl;
	saveFile << copper << std::endl;
	saveFile << gold << std::endl;
	saveFile << platinum << std::endl;
	saveFile.close();
	return 0;
}