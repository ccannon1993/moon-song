/*
*  MoonSong - Conundrum
*  SphericalBounding.h
*  Last Reviewed: 08/05/2014
*/

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <string>

#ifndef SPHERICALBOUNDING_H  
#define SPHERICALBOUNDING_H

class SphericalBounding
{
private:
	glm::vec3 centre;
	glm::vec3 pointOnSphere;
	std::string ID;
	std::string name;	
	float radiusSquared;
	void CalculateRadiusSquared();
	void FindCentre(glm::vec3 minPoint, glm::vec3 maxPoint);
public:
	SphericalBounding(std::string inID, std::string inName);
	~SphericalBounding();
	glm::vec3 GetCentre() {return centre;}	
	float GetRadiusSquared() {return radiusSquared;}
	std::string GetID() { return ID; }
	std::string GetName() { return name; }	
	void SetSB(const std::vector<GLfloat> &verts, int vertCount);	
	void SetPointerSB(const std::vector<GLfloat *> &verts, int vertCount);
	void UpdatePosition(glm::vec3 position);	
	bool DoSBIntersect(SphericalBounding other);
};

#endif