/*
*  MoonSong - Conundrum
*  SphericalBounding.cpp
*  Last Reviewed: 08/05/2014
*  This class is used to set spherical bounding for objects. Spheres should only be used
*  if the object is roughly same size in x, y & z dimensions. Due to the way the radius is
*  found this class will not suit objects that are not similar size in all dimensions.
*  This class allows the spherical bounding to be set after translation/scaling/rotation
*  operations have been carried out. 
*  The class can then calculate the correct radius through its internal functions.
*  The sphere can be moved by updating the centre via the object position.
*  The class can detect collision against another bounding sphere.
*/

#include "sphericalBounding.h"
#include <iostream>

/*
*  Constructor
*  Sets the sphere ID and name.
*/
SphericalBounding::SphericalBounding(std::string inID, std::string inName)
{
	ID = inID;
	name = inName;
}

/*
*  Deconstructor
*/
SphericalBounding::~SphericalBounding()
{
	
}

/*
*  Finds the min, max and centre points on the sphere so can be used to find radiusSquared
*  @param - vector - <GLfloat> takes in verts of object from a vector.
*  @param - int - takes in vertex count for object (NOTE, not indices).
*  Will call function to find radius, distance from point on sphere to centre.
*/
void SphericalBounding::SetSB(const std::vector<GLfloat> &verts, int vertCount)
{		
	pointOnSphere = glm::vec3(99999.99, 99999.99, 99999.99); //min
	glm::vec3 max = glm::vec3(-99999.99, -99999.99, -99999.99);
			
	for(int i = 0; i < vertCount; i += 3)
	{
		//Find min x vertice coordinate to set pointOnSphere
		if( verts[i] < pointOnSphere.x )
		{
			pointOnSphere.x = verts[i];
			pointOnSphere.y = verts[i + 1];
			pointOnSphere.z = verts[i + 2];
		}
		//Find max x vertice coordinate to set max point on sphere
		if( verts[i] > max.x )
		{
				max.x = verts[i];
				max.y = verts[i + 1];
				max.z = verts[i + 2];
		}		
	}	

	//Mid Point Formula
	centre.x = (pointOnSphere.x + max.x)/2;
	centre.y = (pointOnSphere.y + max.y)/2;
	centre.z = (pointOnSphere.z + max.z)/2;
	
	CalculateRadiusSquared();
}

/*
*  Finds the min, max and centre points on the sphere so can be used to find radiusSquared
*  @param - vector - <GLfloat *> takes in verts of object from a vector of pointers
*  due to some objects using pointers.
*  @param - int - takes in vertex count for object (NOTE, not indices).
*  Will call function to find radius, distance from point on sphere to centre.
*/
void SphericalBounding::SetPointerSB(const std::vector<GLfloat *> &verts, int vertCount)
{		
	pointOnSphere = glm::vec3(99999.99, 99999.99, 99999.99); //min
	glm::vec3 max = glm::vec3(-99999.99, -99999.99, -99999.99);
			
	for(int i = 0; i < vertCount; i += 3)
	{
		//Find min x vertice coordinate to set pointOnSphere
		if( *verts[i] < pointOnSphere.x )
		{
			pointOnSphere.x = *verts[i];
			pointOnSphere.y = *verts[i + 1];
			pointOnSphere.z = *verts[i + 2];
		}
		//Find max x vertice coordinate to set max point on sphere
		if( *verts[i] > max.x )
		{
				max.x = *verts[i];
				max.y = *verts[i + 1];
				max.z = *verts[i + 2];
		}		
	}	

	//Mid Point Formula
	centre.x = (pointOnSphere.x + max.x)/2;
	centre.y = (pointOnSphere.y + max.y)/2;
	centre.z = (pointOnSphere.z + max.z)/2;
	
	CalculateRadiusSquared();
}

/*
*  Finds the radiusSquared of the sphere using the centre and point on sphere.
*  Does not use square root to find actual radius as radiusSquared always used in
*  collision detection.
*/
void SphericalBounding::CalculateRadiusSquared()
{
	//Distance formula.
	glm::vec3 dist = pointOnSphere - centre;	
	radiusSquared = dist.x*dist.x + dist.y*dist.y + dist.z*dist.z;		
}

/*
*  Used to update centre in regards to the object position. 
*  @param - glm::vec3 - position of object centre.
*/
void SphericalBounding::UpdatePosition(glm::vec3 position)
{
	centre = position;	
}

/*
*  Checks if this sphere intersects with sphere taken in.
*  For better performance will use the radiusSquared so can avoid
*  square root function.
*  @param - SphericalBounding - used for intersection test.
*  @return - boolean - returns true if spheres intersect, false
*  otherwise.
*/
bool SphericalBounding::DoSBIntersect(SphericalBounding other)
{
	//Distance formula without square root. Distance checked against radii squared.
	glm::vec3 dist = centre - other.centre;	
	float radSquared = radiusSquared + other.radiusSquared;
	if((dist.x*dist.x + dist.y*dist.y + dist.z*dist.z) > (radSquared))
		return false;
	else
		return true;	
}