// Fragment shader computing a spotlight�s effect
#version 330 core

uniform vec3 Ambient;
uniform vec3 LightColor;
uniform vec3 LightPosition;
uniform float Shininess;
uniform float Strength;
uniform vec3 EyeDirection;
uniform float ConstantAttenuation;
uniform float LinearAttenuation;
uniform float QuadraticAttenuation;
uniform vec3 ConeDirection; // adding spotlight attributes
uniform float SpotCosCutoff; // how wide the spot is, as a cosine
uniform float SpotExponent; // control light fall-off in the spot

in vec4 Color;
in vec3 Normal;
in vec4 Position;
out vec4 FragColor;

void main()
{
vec3 lightDirection = LightPosition - vec3(Position);
float lightDistance = length(lightDirection);
lightDirection = lightDirection / lightDistance;
float attenuation = 1.0 /
(ConstantAttenuation + LinearAttenuation * lightDistance + QuadraticAttenuation * lightDistance * lightDistance);
// how close are we to being in the spot?
float spotCos = dot(lightDirection, -ConeDirection);
// attenuate more, based on spot-relative position
if (spotCos < SpotCosCutoff)
attenuation = 0.0;
else
attenuation *= pow(spotCos, SpotExponent);
vec3 halfVector = normalize(lightDirection + EyeDirection);
float diffuse = max(0.0, dot(Normal, lightDirection));
float specular = max(0.0, dot(Normal, halfVector));
if (diffuse == 0.0)
specular = 0.0;
else
specular = pow(specular, Shininess) * Strength;
vec3 scatteredLight = Ambient + LightColor * diffuse * attenuation;
vec3 reflectedLight = LightColor * specular * attenuation;
vec3 rgb = min(Color.rgb * scatteredLight + reflectedLight,
vec3(1.0));
FragColor = vec4(rgb, Color.a);
//FragColor = vec4(1.0);
}