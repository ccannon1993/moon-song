// laserFire.frag
#version 330

precision highp float;

in vec3 ex_V;
in vec3 ex_C;

out vec4 out_Color;

void main(void) {  		
	// Fragment colour
	out_Color = vec4(ex_C, 1.0);
}