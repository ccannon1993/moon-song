// Fragment Shader � file "simple.frag"

#version 330

precision highp float; // needed only for version 1.30
struct simpleLight 
{
vec4 colour;
};

//in  vec4 ex_Color;
out vec4 out_Color;

uniform simpleLight light;

void main(void) {
	out_Color = light.colour;
}
