// laserFire.vert
// Takes in vertex positions and colour.
// Sets vertex positions with matrics and will pass colour to frag shader.
#version 130

uniform mat4 modelview;
uniform mat4 projection;

in vec3 in_Position;
in vec3 in_Colour;

out vec3 ex_V;
out vec3 ex_C;

void main(void) {
	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	
	// colour passed to fragment shader
	ex_C = in_Colour;
	
    gl_Position = projection * vertexPosition;
}