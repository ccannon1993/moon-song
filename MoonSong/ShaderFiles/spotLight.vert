
// Vertex shader for spotlight computed in the fragment shader
#version 330 core
uniform mat4 MVPMatrix;
uniform mat4 MVMatrix;
//uniform mat3 NormalMatrix;

in vec4 VertexColor;
in vec3 VertexNormal;
in vec4 VertexPosition;

out vec4 Color;
out vec3 Normal;
out vec4 Position;

void main()
{

Color = VertexColor;
mat3 NormalMatrix = mat3(MVMatrix);

NormalMatrix = mat3(NormalMatrix);
NormalMatrix = inverse(NormalMatrix);
NormalMatrix = transpose(NormalMatrix);

Normal = normalize(NormalMatrix * VertexNormal);
Normal = VertexNormal;
Position = MVMatrix * VertexPosition;
gl_Position = MVPMatrix * VertexPosition;

}