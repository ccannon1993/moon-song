// Vertex shader for a point-light (local) source, with computation
// done in the fragment shader.
#version 330 core
uniform mat4 MVPMatrix;
uniform mat4 MVMatrix; // now need the transform, minus perspective
//uniform mat3 NormalMatrix;

in vec4 VertexColor;
in vec3 VertexNormal;
in vec4 VertexPosition;

out vec4 Color;
out vec3 Normal;
out vec4 Position; // adding position, so we know where we are
void main()
{

Color = VertexColor;
Color = vec4(1.0, 1.0, 1.0, 1.0);
mat3 NormalMatrix = mat3(MVMatrix);

NormalMatrix = mat3(NormalMatrix);
NormalMatrix = inverse(NormalMatrix);
NormalMatrix = transpose(NormalMatrix);

Normal = normalize(NormalMatrix * VertexNormal);

//Normal = VertexNormal;

Position = MVMatrix * VertexPosition; // pre-perspective space
gl_Position = MVPMatrix * VertexPosition; // includes perspective

}