// Vertex Shader � file "simple.vert"

#version 330

uniform mat4 MVPMatrix;


in  vec4 In_Position;
//out  vec4 ex_Color;
//out vec4 Position;

void main() 
{
	gl_Position = MVPMatrix * In_Position;
}
