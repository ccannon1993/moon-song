// Vertex Shader � file "simple.vert"

#version 330

uniform mat4 MVPMatrix;


in  vec4 In_Position;
in vec4 In_Color;
in vec2 in_TexCoord;

out vec2 ex_TexCoord;
out  vec4 ex_Color;
//out vec4 Position;

void main() 
{
	ex_TexCoord = in_TexCoord;
	//ex_Color = In_Color;
	ex_Color = vec4(1.0);
	gl_Position = MVPMatrix * In_Position;
}
