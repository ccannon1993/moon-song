// Fragment Shader � file "simple.frag"

#version 330

precision highp float; // needed only for version 1.30
struct particle 
{
vec4 colour;
};

uniform sampler2D textureUnit0;
//vec4 colour = vec4(0.5, 1.0, 0.5, 0.0);
in  vec4 ex_Color;
in vec2 ex_TexCoord;

out vec4 out_Color;

uniform particle particle1;

void main(void) {
	
	//ex_Color = vec4(1.0);

	out_Color = ex_Color * texture(textureUnit0, gl_PointCoord);	
	
	if(out_Color.a < 0.5)
	{
	discard;
	}
	
	//out_Color = vec4(0.0,1.0,0.0,1.0);
}
