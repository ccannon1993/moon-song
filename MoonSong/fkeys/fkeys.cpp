/*
*  MoonSong - Conundrum
*  fkeys.cpp
*  Last Reviewed: 08/05/2014
*/

#include "fkeys.h"
#include <iostream>
#include <iterator>

namespace fizz{

	void fizzykeys::update( unsigned char* ikeys )
	{
		keys = ikeys;
	}

	void fizzykeys::update( fizzypop* manager )
	{
		for(unsigned int i = 0; i < activeKeys.size(); i++)
		{
			//std::cout << keys[ activeKeys[i] ];
			if( keys[ activeKeys[i] ] )
				manager->record[ keysToObjs[ activeKeys[i] ] ]->apply( keysToAlts[ activeKeys[i] ] );
		}
		//manager->record[ keysToObjs[ activeKeys[3] ] ]->apply( keysToAlts[ activeKeys[0] ] );
	}

	int fizzykeys::setKey( int key, int ID, fizz* alteration )
	{
		keysToObjs.insert( std::make_pair<int, int>(key, ID) );
		keysToAlts.insert( std::make_pair<int, fizz*>(key, alteration) );
		activeKeys.push_back( key );
		return 0;
	}

	fizz* fizzykeys::getKey( int key )
	{
		return keysToAlts[ key ];
	}

	fizzykeys::fizzykeys(void)
	{
	}

	fizzykeys::~fizzykeys(void)
	{
	}

}