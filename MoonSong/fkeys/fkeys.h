/*
*  MoonSong - Conundrum
*  fkeys.h
*  Last Reviewed: 08/05/2014
*/

#ifndef TEGUKI_SODA_KEYS
#define TEGUKI_SODA_KEYS

#include "../fext/fext.h"
#include <vector>
#include <map>

#define ROTATION 0
#define MOVEMENT 1

#define AXIS 0
#define VIEW 1

namespace fizz{

	class fizzykeys : public fExt
	{
		friend class fizzypop;
	private:
		unsigned char* keys;
		std::vector<int> activeKeys;
		std::map<int, int> keysToObjs;
		std::map<int, fizz*> keysToAlts;
		void update( fizzypop* manager );
	public:
		void update( unsigned char* ikeys );
		int setKey( int key, int ID, fizz* alteration );
		fizz* getKey( int key );
		fizzykeys(void);
		~fizzykeys(void);
	};

}

#endif
