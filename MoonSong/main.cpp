/*
*  MoonSong - Conundrum
*  main.cpp
*  Last Reviewed: 08/05/2014
*/

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "RenderEngine/renderEngine.h"
#include "Quaternion/quaternion.h"
#include "Game/Game.h"

using namespace std;

int main(int argc, char *argv[]) 
{
	Game *game = new Game();
	game->Run();
	game = NULL;
	delete game;
	return 0;
}