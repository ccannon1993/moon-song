/*
*  MoonSong - Conundrum
*  fizz.cpp
*  Last Reviewed: 08/05/2014
*/

#include "../fizzypop/fizzypop.h"
#include <iostream>

namespace fizz{

	int fizz::setPos( glm::vec3 ipos ){ return (!tracked)?(pos = ipos, tracked):(tracked); }
	int fizz::setVel( glm::vec3 ivel ){ return (!tracked)?(mov->vel = ivel, tracked):(tracked); }	
	int fizz::setAcc( glm::vec3 iacc ){ return (!tracked)?(mov->acc = iacc, tracked):(tracked); }
	int fizz::setOri( glm::quat iori ){ return (!tracked)?(ori = iori, tracked):(tracked); }	
	int fizz::setAngVel( glm::quat iangvel ){ return (!tracked)?(rot->angvel = iangvel, tracked):(tracked); }
	int fizz::setAngAcc( glm::quat iangacc ){ return (!tracked)?(rot->angacc = iangacc, tracked):(tracked); }
	glm::vec3 fizz::getPos(){ return pos; }
	glm::vec3 fizz::getVel(){ return mov->vel; }
	glm::vec3 fizz::getAcc(){ return mov->acc; }
	glm::quat fizz::getOri(){ return ori; }
	glm::quat fizz::getAngVel(){ return rot->angvel; }
	glm::quat fizz::getAngAcc(){ return rot->angacc; }

	fizz* fizz::apply(fizz* alteration){
		pos += alteration->pos * ori;
		if( alteration->mov )
		{
			if( !mov ) mov = new movement();
			mov->vel += alteration->mov->vel * ori;
			mov->acc += alteration->mov->acc * ori;
		}
		ori = quat::rotate( ori, alteration->ori );
		if( alteration->rot )
		{
			if( !rot ) rot = new rotation();
			rot->angvel = quat::rotate( quat::rotate( alteration->rot->angvel, alteration->ori ), rot->angvel );
			rot->angacc = quat::rotate( alteration->rot->angacc, rot->angacc );
		}

		return this;
	}


	/*
	*
	*
	*
	*/
	void fizz::update()
	{
		if( mov )
		{
			mov->vel+=mov->acc;
			pos+=mov->vel;
		}
		if( rot )
		{
			rot->angvel = quat::rotate( rot->angvel, rot->angacc );
			ori = quat::rotate( ori, rot->angvel );
		}
	}

	/*
	*
	*
	*
	*/
	void fizz::interpolate( fizz* record, float framePercent )
	{
		mov->vel = record->mov->vel + record->mov->acc * framePercent;
		pos = record->pos + mov->vel * framePercent;
		rot->angvel = quat::interpolate( record->rot->angvel, quat::rotate( record->rot->angvel, record->rot->angacc), framePercent );
		ori = quat::interpolate( record->ori, quat::rotate( record->ori, rot->angvel), framePercent );
	}

	/*
	*
	*
	*
	*/
	fizz* fizz::copy()
	{
		fizz* output = new fizz();

		output->pos = pos;
		if( mov )
		{
			output->mov = new movement();
			output->mov->vel = mov->vel;
			output->mov->acc = mov->acc;
		}else{
			output->mov = nullptr;
		}
		output->ori = ori;
		if( rot )
		{
			output->rot = new rotation();
			output->rot->angvel = rot->angvel;
			output->rot->angacc = rot->angacc;
		}else{
			output->rot = nullptr;
		}

		return output;
	}

	fizz::fizz(void)
	{
		tracked = 0;
		pos = glm::vec3(0.0f);
		mov = new movement();
		mov->vel = glm::vec3(0.0f);
		mov->acc = glm::vec3(0.0f);
		ori = quat::eulerToQuat();
		rot = new rotation();
		rot->angvel = quat::eulerToQuat();
		rot->angacc = quat::eulerToQuat();
	}

	fizz::fizz(int movement_type, int rotation_type)
	{
		tracked = 0;
		pos = glm::vec3(0.0f);
		if( movement_type )
		{
			mov = new movement();
			mov->vel = glm::vec3(0.0f);
			mov->acc = glm::vec3(0.0f);
		}else{
			mov = nullptr;
		}
		ori = quat::eulerToQuat();
		if( rotation_type )
		{
			rot = new rotation();
			rot->angvel = quat::eulerToQuat();
			rot->angacc = quat::eulerToQuat();
		}else{
			rot = nullptr;
		}
	}

	fizz::~fizz(void)
	{
		if( mov )
		{
			delete mov;
		}
		if( rot )
		{
			delete rot;
		}
	}
}