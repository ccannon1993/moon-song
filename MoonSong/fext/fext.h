/*
*  MoonSong - Conundrum
*  fext.h
*  Last Reviewed: 08/05/2014
*/

#ifndef TEGUKI_SODA_EXT_ABS
#define TEGUKI_SODA_EXT_ABS

#include"../fizzypop/fizzypop.h"

namespace fizz{

	class fExt
	{
	protected:
	public:
		virtual void update(fizzypop*) = 0;
	};

}

#endif