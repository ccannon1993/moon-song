/*
*  MoonSong - Conundrum
*  BitmapLoader.h
*  Last Reviewed: 08/05/2014
*/

#pragma once
#include<GL\glew.h>
#include<iostream>
#include<SDL.h>

class BitmapLoader
{
public:
	BitmapLoader(void);
	~BitmapLoader(void);
	GLuint loadBitmap(const char *fname);
};

